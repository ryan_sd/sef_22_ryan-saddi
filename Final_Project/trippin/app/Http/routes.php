<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function (){
	return redirect('/home');
});

Route::get('/home', 'HomeController@index');

Route::get('/cities', 'HomeController@listCities');

Route::post('/cities', 'SelectTripsController@searchCity');

Route::get('/trips/{city}', 'SelectTripsController@getOptions')
	->where('city', '[a-zA-Z]+( [a-zA-Z]+)*');

Route::get('/trips/{city}/{id}', 'TripScheduleController@getDaysFromTrip')
	->where(array('city' => '[a-zA-Z]+( [a-zA-Z]+)*', 'id' => '[0-9]+'));

Route::get('/trips/{id}/days/{num}', 'TripScheduleController@getLandmarksFromDay')
 	->where(array('id' => '[0-9]+', 'num' => '[0-9]+'));

Route::get('/profile', 'ProfileController@index');

Route::post('/profile', 'ProfileController@saveTrip');

Route::auth();

