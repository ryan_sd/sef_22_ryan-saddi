<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\City;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imagePath = config('app.cities_path');
        // select 3 random cities
        $randomCities = City::orderByRaw('RAND()')->take(6)->get();
        return view('home', ['random' => $randomCities, 'imagePath' => $imagePath]);
    }

    /**
     * [FunctionName description]
     * @param string $value [description]
     */
    public function listCities(Request $request)
    {
        $look = $request->input('term');

        // get the first 5 matching records that contain search term
        $citiesList = City::where('city_name', 'like', '%'.$look.'%')
                          ->limit(5)
                          ->get();
        
        $citiesJson = [];
        // prepare every matching record to be autocomplete readable
        foreach ($citiesList as $city) {
            $label = $city->city_name . ", " .$city->country->country_name;
            $cityJson = ["value" => $city->city_name, "label" => $label];
            // populate response
            $citiesJson []= $cityJson;
        }
        return response()->json($citiesJson, 200);
    }
}
