<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Trip;
use App\TripDay;
use App\VisitPurpose;
use App\DayPlan;

class TripScheduleController extends Controller
{
    public function getDaysFromTrip($city, $tripId)
    {
    	$trip = Trip::find($tripId);

    	$days = TripDay::where('trip_id', $tripId)
    					->orderBy('sequence')
    					->get();

    	$purposes = VisitPurpose::all();

    	return view('trip-schedule', ['trip' => $trip,
    								  'days' => $days,
    								  'purposes' => $purposes]);
    }
    
    public function getLandmarksFromDay($tripId, $day)
    {
    	// retrieve the id for the desired trip day
    	$dayId = TripDay::where('trip_id', $tripId)
    					->where('sequence', $day)
    					->pluck('id')->first();
    	// if no trip day exists
    	if (is_null($dayId)) {
	    	return response()->json(['code' => 110,
	    							 'message' => 'trip day not found',
	    							 'description' => 'day ' . $day . 
	    							 				  ' in trip ' . $tripId .
	    							 				  ' does not exist.'],
	    							 404);
    	} else {
    		// get the day plan for the found day
	    	$dayPlanRecords = DayPlan::where('trip_day_id',$dayId)
	    							 ->orderBy('start')
	    							 ->get();

	    	// array that will be containing a list of
	    	// geoJson landmarks, readable for mapBox
	    	$dayPlan = [];
	    	//populate dayPlan with geoJson
	    	foreach ($dayPlanRecords as $visit) {
	    		// fields to define a landmark marker in mapBox
	    		$landmark = ['type' => 'Feature', 'geometry' => ['type' => 'Point']];

	    		// properties field for a landmark marker
	    		$propertyForLandmark = json_decode($visit->landmark);
	    		$propertyForLandmark->start = $visit->start;
	    		$propertyForLandmark->end = $visit->end;
	    		$propertyForLandmark->{'marker-size'} = "large";

	    		switch ($propertyForLandmark->visit_purpose_id) {
	    			case '4':
	    				$propertyForLandmark->{'marker-color'} = "307bbb";
	    				break;
					case '2':
	    				$propertyForLandmark->{'marker-color'} = "008000";
	    				break;
					case '3':
	    				$propertyForLandmark->{'marker-color'} = "FF0000";
	    				break;
	    			case '1':
	    				$propertyForLandmark->{'marker-color'} = "dd9d22";
	    				break;
	    			
	    			default:
	    				break;
	    		}

	    		$landmark['properties'] = $propertyForLandmark;

	    		// coordinates to place in map
	    		$landmark['geometry']['coordinates'] = array($propertyForLandmark->long,
	    													 $propertyForLandmark->lat);
	    		// add to the list
	    		$dayPlan []= $landmark;
	    	}

	    	return response()->json($dayPlan, 200);
	    }
    }
}
