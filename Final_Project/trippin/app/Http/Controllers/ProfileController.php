<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\History;
use App\Trip;
use App\City;

class ProfileController extends Controller
{
	/**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * [saveTrip description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function saveTrip(Request $request)
    {

        $this->validate($request, ['tripId' => 'required',]);
        $tripId = $request->input('tripId');
    	$userId = Auth::user()->id;

        // look for existing record in history table
    	$tripHistoryFound = History::where('trip_id', $tripId)
    							   ->where('user_id', $userId)
    							   ->exists();
        // check if trip doesn't exists
    	if (!$tripHistoryFound) {
	    	$history = new History;
			$history->trip_id = $tripId;
			$history->user_id = $userId;
			$history->save();

            // get the user's prefered type of trip
			$preferedId = $this->analyzePreference();
            // update user's preference accordingly
    		$this->updatePreference($preferedId);
		}
		

		return $this->index();
    }

    /**
     * [analyzePreference description]
     * @return [type] [description]
     */
    private function analyzePreference()
    {
    	$userId = Auth::user()->id;
    	$tripsHistory = History::where('user_id', $userId)
    							   ->get();

        // user's score for each preference
    	$userChoices = [0, 0, 0, 0];
        // populate user's score on each type of each trip
    	foreach ($tripsHistory as $savedTrip) {
    		$trip = $savedTrip->trip;
    		foreach ($trip->tripCharacteristics as $characteristic) {
    			$charId = $characteristic->visit_purpose_id;
    			$userChoices[$charId-1] += $characteristic->percentage;	
    		}
    	}
        // get the best "score" (+1 for index match)
    	$preferedId = array_search(max($userChoices), $userChoices) + 1;
    	
    	return $preferedId;
    }

    /**
     * [updatePreference description]
     * @param  [type] $preferenceId [description]
     * @return [type]               [description]
     */
    private function updatePreference($preferenceId)
    {
    	$user = Auth::user();
    	$user->visit_purpose_id = $preferenceId;
    	$user->save();
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $imagePath = config('app.trips_path');

    	$user = Auth::user();
    	$history = History::where('user_id', $user->id)
    					  ->orderBy('created_at','desc')
    					  ->get();

    	$userPreference = null;
    	if (!is_null($user->visit_purpose_id)) {
			$userPreference = $user->visitPurpose->purpose;
		}

    	return view('profile', ['user' => $user,
    							'savedTrips' => $history,
    							'preference' => $userPreference,
                                'imagePath' => $imagePath,]);
    }
}
