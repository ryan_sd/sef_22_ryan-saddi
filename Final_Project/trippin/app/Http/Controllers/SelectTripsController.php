<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\City;
use App\Country;
use App\Trip;
use Auth;
use App\TripCharacteristic;

class SelectTripsController extends Controller
{
    /**
     * retrieve all remaining cities that belong to the same country of the selected city
     * @param  [type] $countryId     [description]
     * @param  [type] $currentCityId [description]
     * @return [type]                [description]
     */
    private function getCitiesInCountry($countryId, $currentCityId)
    {
    	return City::where('country_id', $countryId)
    			   ->where('id', '!=',   $currentCityId)
                   ->get();
    }

    /**
     * [getOptions description]
     * @param  [type] $cityName [description]
     * @return [type]           [description]
     */
    public function getOptions($cityName)
    {
        $cityImagesPath = config('app.cities_path');
        $tripImagesPath = config('app.trips_path');
        $coverImagesPath = config('app.covers_path');

    	$city = City::where('city_name', $cityName)->first();
    	if (is_null($city)) {
            return abort(404, $cityName . ' not found.');
    	} else {
            $trips = $city->trips;
            $cities = $this->getCitiesInCountry($city->country_id, $city->id);
            
            $preferedTrip = null;
            // if user is logged in,
            if (Auth::user()) {
                // get his prefered type, if exists
                $preferedId = Auth::user()->visit_purpose_id;
                if (!is_null($preferedId)) {
                    $preferedTrip = $this->findPreferedTrip($preferedId, $trips);
                }
            }
    		return view('select-trip', ['trips' => $trips,
                                        'cities' => $cities,
                                        'current' => $city,
                                        'preferedTrip' => $preferedTrip,
                                        'cityPath' => $cityImagesPath,
                                        'tripPath' => $tripImagesPath,
                                        'coverPath' => $coverImagesPath,]);
    	}
    }
    /**
     * [findPreferedTrip description]
     * @param  [type] $preference [description]
     * @param  [type] $trips      [description]
     * @return [type]             [description]
     */
    public function findPreferedTrip($preference, $trips)
    {
        $preferedTrip = null;
        $maxPreference = 25;
        foreach ($trips as $trip) {
            $percentage = TripCharacteristic::where('trip_id', $trip->id)
                                            ->where('visit_purpose_id', $preference)
                                            ->first()
                                            ->percentage;
            if ($percentage == 100) {
                return $trip;
            }
            if ($percentage > $maxPreference) {
                $maxPreference = $percentage;
                $preferedTrip = $trip;
            }
        }
        return $preferedTrip;
    }

    /**
     * [searchCity description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function searchCity(Request $request)
    {
        $this->validate($request, ['city' => 'required',]);
        $city = $request->input('city');
        return redirect('trips/' . $city);
    }
}
