<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayPlan extends Model
{
    public function landmark()
    {
    	return $this->belongsTo('App\Landmark');
    }

    public function tripDay()
    {
    	return $this->belongsTo('App\TripDay');
    }
}
