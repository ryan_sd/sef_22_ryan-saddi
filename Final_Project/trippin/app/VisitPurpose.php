<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitPurpose extends Model
{
    public function users()
    {
    	return $this->hasMany('App\User');
    }

    public function tripCharacteristics()
    {
    	return $this->hasMany('App\TripCharacteristic');
    }
    public function landmarks()
    {
    	return $this->hasMany('App\Landmark');
    }
}
