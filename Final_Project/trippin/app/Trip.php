<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    public function tripDays()
    {
    	return $this->hasMany('App\TripDay');
    }

    public function tripCharacteristics()
    {
    	return $this->hasMany('App\TripCharacteristic');
    }

    public function histories()
    {
    	return $this->hasMany('App\History');
    }

    public function city()
    {
    	return $this->belongsTo('App\City');
    }
}
