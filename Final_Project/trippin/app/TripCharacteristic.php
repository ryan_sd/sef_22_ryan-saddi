<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripCharacteristic extends Model
{
    public function trip()
    {
    	return $this->belongsTo('App\Trip');
    }

    public function visitPurpose()
    {
    	return $this->belongsTo('App\VisitPurpose');
    }
}
