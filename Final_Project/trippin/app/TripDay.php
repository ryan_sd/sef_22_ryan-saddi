<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripDay extends Model
{
    public function dayPlans()
    {
    	return $this->hasMany('App\DayPlan');
    }

    public function trip()
    {
    	return $this->belongsTo('App\Trip');
    }
}
