<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landmark extends Model
{
    public function dayPlans()
    {
    	return $this->hasMany('App\DayPlan');
    }

    public function visitPurpose()
    {
    	return $this->belongsTo('App\VisitPurpose');
    }

    public function city()
    {
    	return $this->belongsTo('App\City');
    }
}
