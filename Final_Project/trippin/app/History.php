<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    public function trip()
    {
    	return $this->belongsTo('App\Trip');
    }

    public function user()
    {
    	return $this->belongsTo('App\Trip');
    }
}
