<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Country;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $countries = Country::all();
        view()->share('countries', $countries);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
