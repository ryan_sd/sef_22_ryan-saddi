var focusedIndex = -1;
var map = null;
var markers = [];
var layerGroup = null;

$(window).load(function() {
    L.mapbox.accessToken = 'pk.eyJ1IjoicnlhbnNkIiwiYSI6ImNpb24xY3FsY' +
                           'jAwMjJ3aG05eThsZThoc20ifQ.NToA28m9RmE-ddPrAnzX7w';

    var zoom = 5;
    map = L.mapbox.map('map',
                       'mapbox.streets',
                       { zoomControl: true }).setView([-35.792529592717,
                                                        129.89108613555], zoom);
    layerGroup = L.layerGroup().addTo(map);

    $('li.day').on('click', selectDay);

    $('#next').on('click', nextMarker);
    $('#prev').on('click', prevMarker);

    $($('li.day')[0]).trigger('click');
});

function selectDay() {
    // remove previous selected
    $('li.day').removeClass('active');
    // display selected
    $(this).addClass("active");
    //get selected day number
    var dayNumber = $('li.day.active span').text();
    // load selected day on map
    loadDay(dayNumber);
}

function loadDay(day) {
    var tripId = $('#trip').attr('trip-id');

    $.ajax({
        url: APP_URL + "/trips/" + tripId + "/days/" + day,
        success: function(json) {
            var geoJson = {
                type: "FeatureCollection",
                features: json
            };

            resetMap();
            presentDayStats(day, json);
            drawRoute(geoJson);
            markersSetUp(geoJson);
        },
        error: function (json) {
            console.log("failed");
        }
    });
}


function resetMap() {
    // default values
    markers = [];
    focusedIndex = -1;
    // remove all markers and routes from map
    layerGroup.clearLayers();
}

function presentDayStats(dayNumber, landmarksGeoJson) {
    var totalCost = 0;
    var dayCharacteristics = [0, 0, 0, 0];
    var landmarksCount = landmarksGeoJson.length;

    var startDay = landmarksGeoJson[0].properties.start;
    var endDay = landmarksGeoJson[landmarksCount-1].properties.end;

    // count each charactersitic for day
    for (var i = 0; i < landmarksCount; i++) {
        totalCost += parseInt(landmarksGeoJson[i].properties.cost);
        var visitPurposeIndex = landmarksGeoJson[i].properties.visit_purpose_id;
        dayCharacteristics[parseInt(visitPurposeIndex) - 1]++;
    }
    // change characteristics to percentages
    var percentages = getDayPercentages(dayCharacteristics, landmarksGeoJson.length);
    // display the new percentages on view
    updatePercentageView(percentages);
    // present day info in description box
    var content = '<h4 id="day-number">Day ' + dayNumber + '</h4>' +
                  '<p id="day-time">Your trip will start at ' + startDay +
                  ' and ends at ' + endDay + '</p>' +
                  '<p id="day-cost">It is estimated that you will spend ' +
                   totalCost + '$ in total on that day.</p>';
    $('#info').html(content);
}

function drawRoute(geoJson) {
    // get number of markers
    var markerCount = geoJson.features.length;
    // if there are enough markers to draw a route
    if (markerCount > 2) {
        // set the start of the journey
        var origin = geoJson.features[0];
        // set the end of the journey
        var destination = geoJson.features[markerCount-1];

        // create a directions object
        var directions = L.mapbox.directions();
        // add its layer and control to the map
        directionsLayer = L.mapbox.directions.layer(directions, {readonly: true, routeStyle: {color: '#FFFF00', weight: 4, opacity: 1}}).addTo(layerGroup);
        var directionsRoutesControl = L.mapbox.directions.routesControl('routes', directions).addTo(layerGroup);
        
        directions.setOrigin(origin).setDestination(destination);

        // loop through the middle markers and add to the route
        for (var i = 1; i < markerCount-1; i++) {
            directions.addWaypoint(i, geoJson.features[i]);
        }
        // draw the route on map
        directions.query();
    }  
}

function markersSetUp(geoJson) {
    var markersLayer = L.mapbox.featureLayer().addTo(layerGroup);
    markersLayer.on('layeradd', function(e) {
        var marker = e.layer;
        var feature = marker.feature;
        // Create custom popup content
        var popupContent =  '<h4>' + feature.properties.landmark_name + '</h4>' + 
                            '<p>'  + feature.properties.start + ' - ' + 
                            feature.properties.end + '</p>';
        marker.bindPopup(popupContent,{closeButton: false});
    });
    // set markersJson to layer
    markersLayer.setGeoJSON(geoJson);
    // focus map on all markers
    map.fitBounds(markersLayer.getBounds());
    markersLayer.on('mouseover', function(e) {
        e.layer.openPopup();
    });
    markersLayer.on('mouseout', function(e) {
        e.layer.closePopup();
    });

    markersLayer.on('click', function (e) {
        var location = e.layer;
        focusedIndex = geoJson.features.indexOf(location.feature);
        describeMarker(location);
    });

    markersLayer.eachLayer(function(marker) { markers.push(marker); });
}

function prevMarker() {
    if (focusedIndex > 0) {
        focusedIndex--;
        var marker = markers[focusedIndex];
        describeMarker(marker);
    } else {
        // get the previous day element in scroll bar (extra -1 for index)
        var previousIndex = parseInt($('li.day.active span').text()) - 2;
        if (previousIndex >= 0) {
            var prevDay = $('li.day')[previousIndex];
            $(prevDay).trigger('click');
        }
    }
}

function nextMarker() {
    if (focusedIndex < markers.length-1) {
        focusedIndex++;
        var marker = markers[focusedIndex];
        describeMarker(marker);
    } else {
        var nextIndex = parseInt($('li.day.active span').text());
        var daysCount = $('li.day').length;
        if (nextIndex < daysCount) {
            var nextDay = $('li.day')[nextIndex];
            $(nextDay).trigger('click');
        }
    }
}

function describeMarker(location) {
    //default zoom level for marker 
    var zoomMarker = 13;
    //if user zoomed closer to marker, change default
    if (map.getZoom() > 13) {
        zoomMarker = map.zoom;
    }
    //pan map to the described marker
    map.setView(location.getLatLng(), zoomMarker);
    //open tooltip for marker
    location.openPopup();
    //update content in description bar (below map)
    var content = '<h4>' + location.feature.properties.landmark_name +
                  ' - ' + location.feature.properties.cost + '$</h4>' +
                  '<p>' + location.feature.properties.landmark_description + '</p>' +
                  '<p>' + location.feature.properties.start + ' - ' + 
                    location.feature.properties.end  +'</p>' +
                  '<p>' + (focusedIndex+1) + ' / ' + markers.length + '</p>';
    $('#info').html(content);
}

function getDayPercentages(characteristics, visitsCount) {
    var percentages = [];
    for (var i = 0; i < characteristics.length; i++) {
        percentages[i] = parseInt(characteristics[i] * 100 / visitsCount);
    }
    return percentages;
}

function updatePercentageView(percentages) {
    for (var i = 0; i < percentages.length; i++) {
        var percentageElement = $($('span.percentage')[i]);
        percentageElement.text(percentages[i]);
        var circleParent = percentageElement.parent().parent();

        var lastPercentage = circleParent.attr('class').split(' ').pop();
        circleParent.removeClass(lastPercentage).addClass('p'+percentages[i]);
    }
}