@extends('layouts.app')

@section('head-data')
<title>Trippin - Home</title>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@endsection

@section('content')


<div id="banner" class="row no-margins" style="background-image: url('{{ URL::asset('assets/img/cover.jpg') }}')">
    <div class="col-xs-6 col-xs-offset-3">
        <h2>Choose Your Destination</h2>
        @if (count($errors) > 0)
            <div class="alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open(array('url' => '/cities')) !!}
            {!! Form::text('city', '', array('class' => 'col-xs-8', 'placeholder' => 'City...', 'id' => 'tags',)) !!}
            {!! Form::submit('Trip It!', array('class' => 'col-xs-3 col-xs-offset-1')) !!}
        {!! Form::close() !!}
    </div>
</div>

<div id="contain" class="container">
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <h2><span>Discover The World!</span></h2>
            @foreach($random as $randCity)
            <div class="col-md-4 col-sm-6 space">
                <div class="trip-thumbnail">
                    <a href="{{ URL::to('/trips/' . $randCity->city_name) }}">
                        <div class="trip-header">
                            <img class="img-responsive" alt="{{ $randCity->thumbnail }}" src="{{ URL::asset('assets/'. $imagePath .  $randCity->thumbnail) }}">
                            <p class="trip-title">{{ $randCity->city_name . ', ' . $randCity->country->country_name}}</p>
                        </div>
                    </a>
                    <div class="trip-desc">
                        {{ count($randCity->landmarks) }} Attractions to Visit!
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
            // var availableTags = [
            //     "Beirut, Lebanon",
            //     "Rome, Italy",
            //     "Venice, Italy",
            //     "New York, USA",
            //     "Paris, France",
            // ];
            $( "#tags" ).autocomplete({
                source: "./cities?term=" + $("#tags").text()
            });
        });
</script>
@endsection