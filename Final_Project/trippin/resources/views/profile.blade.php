@extends('layouts.app')

@section('head-data')
<title>Trippin - My Profile</title>
@endsection

@section('content')
<div id="banner" class="row no-margins" style="background-image: url('{{ URL::asset('assets/img/cover.jpg') }}')">
	<div class="col-xs-4 col-xs-offset-4">
		<h2>{{ $user->name }}</h2>
		@if(!is_null($preference))
				<p>My favorite trip is <span style="color:yellow;">{{ $preference }}</span></p>
		@endif
	</div>
</div>
<div id="contain" class="container">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
		@if(count($savedTrips) > 0)
			<h2><span>The Trips I Saved</span></h2>
			@foreach($savedTrips as $savedTrip)
			<div class="col-md-4 col-sm-6 space">
				<div class="trip-thumbnail">
					<a href="{{ URL::to('/trips/' . $savedTrip->trip->city->city_name .'/' . $savedTrip->trip_id) }}">
						<div class="trip-header">
							<img class="img-responsive" alt="{{ $savedTrip->trip->thumbnail }}" src="{{ URL::asset('assets/'.$imagePath.$savedTrip->trip->thumbnail) }}">
							<p class="trip-title">{{ $savedTrip->trip->trip_name }}</p>
						</div>
					</a>
					<div class="trip-desc">
						{{ substr($savedTrip->trip->trip_description, 0, 100) . '...' }}
					</div>
				</div>
			</div>
			@endforeach
		@else
			<h2 id="no-trips"><span>I haven't Saved any trips yet</span></h2>
		@endif
		</div>
	</div>
</div>
@endsection