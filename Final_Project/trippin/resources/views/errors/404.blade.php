@extends('layouts.app')

@section('content')
<div id="banner" class="row no-margins" style="background-image: url('{{ URL::asset('assets/img/cover.jpg') }}')">
    <div class="col-xs-4 col-xs-offset-4"><h2>{{ $exception->getMessage() }}</h2></div>
</div>

@endsection