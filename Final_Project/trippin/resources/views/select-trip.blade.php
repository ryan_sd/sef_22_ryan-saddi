@extends('layouts.app')

@section('head-data')
<title>Trippin - {{ $current->city_name }}</title>
@endsection

@section('content')
<div id="banner" class="row no-margins" style="background-image: url({{ URL::asset('assets/'.$coverPath . $current->cover) }});">
	<div class="col-xs-4 col-xs-offset-4"><h2>&quot;{{ $current->greetings }}&quot;</h2></div>
</div>
<div id="contain" class="container">
	<div class="col-md-10 col-md-offset-1">
		<div class="row">
			<h2><span>Different Guides to Explore {{ $current->city_name }}</span></h2>
			@foreach($trips as $trip)
			<div class="col-md-4 col-sm-6 space">
				<div class="trip-thumbnail">
					<a href="{{ URL::to('/trips/' . $current->city_name .'/' . $trip->id) }}">
						<div class="trip-header">
							<img class="img-responsive" alt="{{ $trip->thumbnail }}" src="{{ URL::asset('assets/'.$tripPath . $trip->thumbnail) }}">
							<p class="trip-title">{{ $trip->trip_name }}</p>
						</div>
					</a>
					<div class="trip-desc">
						{{ substr($trip->trip_description, 0, 100) . '...' }}
					</div>
					<div class="clearfix">
						@foreach($trip->tripCharacteristics as $stat)
						<div class="c100 p{{ $stat->percentage }} small {{ $stat->visitPurpose->purpose }}">
							<span><span>{{ $stat->percentage }}</span>%
							<br>{{ $stat->visitPurpose->purpose }}</span>
							<div class="slice">
								<div class="bar"></div>
								<div class="fill"></div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
			@endforeach
		</div>
		@if(!is_null($preferedTrip))
		<div class="row">
			<h2><span>We Think You're Going to Like</span></h2>
			<div class="col-md-4 col-md-offset-4 col-sm-6 space col-sm-offset-3">
				<div class="trip-thumbnail">
					<a href="{{ URL::to('/trips/' . $current->city_name .'/' . $preferedTrip->id) }}">
						<div class="trip-header">
							<img class="img-responsive" alt="{{ $preferedTrip->thumbnail }}" src="{{ URL::asset('assets/'.$tripPath . $preferedTrip->thumbnail) }}">
							<p class="trip-title">{{ $preferedTrip->trip_name }}</p>
						</div>
					</a>
					<div class="trip-desc">
						{{ substr($preferedTrip->trip_description, 0, 100).'...' }}
					</div>
					<div class="clearfix">
						@foreach($preferedTrip->tripCharacteristics as $stat)
						<div class="c100 p{{ $stat->percentage }} small {{ $stat->visitPurpose->purpose }}">
							<span><span>{{ $stat->percentage }}</span>%
							<br>{{ $stat->visitPurpose->purpose }}</span>
							<div class="slice">
								<div class="bar"></div>
								<div class="fill"></div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
		@endif
		@if(!$cities->isEmpty())
		<div class="row">
			<h2><span>Can't Get Enough of {{ $current->country->country_name }}?</span></h2>
			@foreach($cities as $city)
			<div class="col-md-4 col-sm-6 space">
				<div class="trip-thumbnail">
					<a href="{{ URL::to('/trips/'.$city->city_name) }}">
						<div class="trip-header">
							<img class="img-responsive" alt="{{ $city->thumbnail }}" src="{{ URL::asset('assets/'.$cityPath .  $city->thumbnail) }}">
							<p class="trip-title">{{ $city->city_name }}</p>
						</div>
					</a>
					<div class="trip-desc">
						{{ count($city->landmarks) }} Attractions to Visit!
					</div>
				</div>
			</div>
			@endforeach
		</div>
		@endif
	</div>
</div>
@endsection