@extends('layouts.app')

@section('head-data')
<title>Trippin - {{ $trip->city->city_name }}</title>
<script src='https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.js'></script>
<link href='https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.css' rel='stylesheet'/>
<script src='https://api.mapbox.com/mapbox.js/plugins/mapbox-directions.js/v0.4.0/mapbox.directions.js'></script>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox.js/plugins/mapbox-directions.js/v0.4.0/mapbox.directions.css' type='text/css' />
<script type="text/javascript" src="{{ URL::asset('assets/js/main.js') }}"></script>
@endsection

@section('content')
<div class="container">
	<div class="col-sm-10 col-sm-offset-1">
		<h2 id="trip" trip-id="{{ $trip->id }}"><span>{{ $trip->trip_name }}</span></h2>
		<p id="trip-description">{{ $trip->trip_description }}</p>
	</div>
</div>
	<div class="col-sm-2 full-bar">
		<ul id="days" class="nav nav-pills nav-stacked">
			@foreach($days as $day)
			<li role="presentation" class="day"><a role="button">Day <span>{{ $day->sequence }}</span></a></li>
			@endforeach
		</ul>
	</div>
	<div class="col-sm-9 full-bar">
		<div id="map"></div>
		<div id="description">
			<div id="info"></div>

			<a id="prev" class="left carousel-control" role="button">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a id="next" class="right carousel-control" role="button">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><br/></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<div class="col-sm-1 full-bar">
		<div class="col-sm-12" id="stats">
		@foreach($purposes as $stat)
			<div class="c100 {{ $stat->purpose }} p0">
				<span><span class="percentage">0</span>%<br>{{ $stat->purpose }}</span>
				<div class="slice">
					<div class="bar"></div>
					<div class="fill"></div>
				</div>
			</div>
		@endforeach
		</div>
		{!! Form::open(array('url' => '/profile')) !!}
            {{ Form::hidden('tripId', $trip->id) }}
            {!! Form::submit('Save Trip!', array('class' => 'col-xs-10 col-xs-offset-1')) !!}
        {!! Form::close() !!}
	</div>
<script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
</script>
@endsection