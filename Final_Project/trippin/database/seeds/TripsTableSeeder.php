<?php

use Illuminate\Database\Seeder;

class TripsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trips')->insert([
            [
             'trip_name' => 'Everywhere In Beirut',
             'trip_description' => 'This trip takes you to the fanciest places for shopping and food. And it always ends your day with a god drink at Beirut\'s best clubs and bars.', 'city_id' => '1', 'thumbnail' => 'beirut_night.jpg',
            ],[
             'trip_name' => 'Must-See New York',
             'trip_description' => 'This trip is focused on the best of New York City attractions and all the well-known landmarks such as the Times Square, Central Park, Statue of Liberty and many other must-see attractions. You will also enjoy some of the best shopping opportunities that New York City has to offer.', 'city_id' => '6',
             'thumbnail' => 'liberty.jpg',
            ],[
             'trip_name' => 'Beirut Top Attractions',
             'trip_description' => 'This trip circles around Beirut\'s "Must Sees". Everyday will include a quick meal with a lot of sight seeing into Beirut\'s history.', 'city_id' => '1', 'thumbnail' => 'raouche.jpg',
            ],[
             'trip_name' => 'Architecture of Beirut',
             'trip_description' => 'Discover Beirut\'s unique oriental architecture. Also have the chance to see the best of Beiru\'s art', 'city_id' => '1', 'thumbnail' => 'beirut_archi.jpg',
            ],
        ]);
    }
}
