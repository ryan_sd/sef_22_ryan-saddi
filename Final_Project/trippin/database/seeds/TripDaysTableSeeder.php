<?php

use Illuminate\Database\Seeder;

class TripDaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trip_days')->insert([
            ['sequence' => '1', 'trip_id' => '1'],
            ['sequence' => '2', 'trip_id' => '1'],
            ['sequence' => '3', 'trip_id' => '1'],

            ['sequence' => '1', 'trip_id' => '3'],
            ['sequence' => '2', 'trip_id' => '3'],
            
            ['sequence' => '1', 'trip_id' => '2'],
            ['sequence' => '2', 'trip_id' => '2'],
            ['sequence' => '3', 'trip_id' => '2'],
            ['sequence' => '4', 'trip_id' => '2'],
            ['sequence' => '5', 'trip_id' => '2'],
            ['sequence' => '6', 'trip_id' => '2'],

        ]);
    }
}