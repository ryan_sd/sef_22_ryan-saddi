<?php

use Illuminate\Database\Seeder;

class DayPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('day_plans')->insert([
        	[
        	 'start' => '11:00',
             'end' => '11:30',
        	 'trip_day_id' => '6',
        	 'landmark_id' => '2',
        	],[
        	 'start' => '12:05',
             'end' => '13:00',
        	 'trip_day_id' => '6',
        	 'landmark_id' => '3',
        	],[
        	 'start' => '11:35',
             'end' => '12:00',
        	 'trip_day_id' => '6',
        	 'landmark_id' => '4',
        	],[
        	 'start' => '13:30',
             'end' => '16:00',
        	 'trip_day_id' => '6',
        	 'landmark_id' => '5',
        	],[
        	 'start' => '12:00',
             'end' => '13:30',
        	 'trip_day_id' => '7',
        	 'landmark_id' => '6',
        	],[
        	 'start' => '13:40',
             'end' => '15:00',
        	 'trip_day_id' => '7',
        	 'landmark_id' => '7',
        	],[
        	 'start' => '17:00',
             'end' => '17:30',
        	 'trip_day_id' => '7',
        	 'landmark_id' => '8',
        	],[
        	 'start' => '16:45',
             'end' => '17:00',
        	 'trip_day_id' => '7',
        	 'landmark_id' => '9',
        	],[
        	 'start' => '15:30',
             'end' => '16:30',
        	 'trip_day_id' => '7',
        	 'landmark_id' => '10',
        	],[
        	 'start' => '17:45',
             'end' => '19:00',
        	 'trip_day_id' => '7',
        	 'landmark_id' => '11',
        	],[
        	 'start' => '19:15',
             'end' => '20:30',
        	 'trip_day_id' => '7',
        	 'landmark_id' => '12',
        	],[
        	 'start' => '10:00',
             'end' => '11:00',
        	 'trip_day_id' => '8',
        	 'landmark_id' => '13',
        	],[
        	 'start' => '11:30',
             'end' => '12:30',
        	 'trip_day_id' => '8',
        	 'landmark_id' => '14',
        	],[
        	 'start' => '12:40',
             'end' => '13:20',
        	 'trip_day_id' => '8',
        	 'landmark_id' => '15',
        	],[
        	 'start' => '14:00',
             'end' => '16:00',
        	 'trip_day_id' => '8',
        	 'landmark_id' => '16',
        	],[
        	 'start' => '4:00',
             'end' => '00:00',
        	 'trip_day_id' => '9',
        	 'landmark_id' => '17',
        	],[
        	 'start' => '1:00',
             'end' => '00:00',
        	 'trip_day_id' => '9',
        	 'landmark_id' => '18',
        	],[
        	 'start' => '2:00',
             'end' => '00:00',
        	 'trip_day_id' => '9',
        	 'landmark_id' => '19',
        	],[
        	 'start' => '3:00',
             'end' => '00:00',
        	 'trip_day_id' => '9',
        	 'landmark_id' => '20',
        	],[
        	 'start' => '4:00',
             'end' => '00:00',
        	 'trip_day_id' => '9',
        	 'landmark_id' => '21',
        	],[
        	 'start' => '5:00',
             'end' => '00:00',
        	 'trip_day_id' => '9',
        	 'landmark_id' => '22',
        	],[
        	 'start' => '1:00',
             'end' => '00:00',
        	 'trip_day_id' => '10',
        	 'landmark_id' => '23',
        	],[
        	 'start' => '2:00',
             'end' => '00:00',
        	 'trip_day_id' => '10',
        	 'landmark_id' => '24',
        	],[
        	 'start' => '3:00',
             'end' => '00:00',
        	 'trip_day_id' => '10',
        	 'landmark_id' => '25',
        	],[
        	 'start' => '4:00',
             'end' => '00:00',
        	 'trip_day_id' => '10',
        	 'landmark_id' => '26',
        	],[
        	 'start' => '1:00',
             'end' => '00:00',
        	 'trip_day_id' => '11',
        	 'landmark_id' => '27',
        	],[
        	 'start' => '2:00',
             'end' => '00:00',
        	 'trip_day_id' => '11',
        	 'landmark_id' => '28',
        	],[
             'start' => '12:30',
             'end' => '14:30',
             'trip_day_id' => '1',
             'landmark_id' => '31',
            ],[
             'start' => '15:00',
             'end' => '17:00',
             'trip_day_id' => '1',
             'landmark_id' => '30',
            ],[
             'start' => '17:30',
             'end' => '18:30',
             'trip_day_id' => '1',
             'landmark_id' => '1',
            ],[
             'start' => '22:00',
             'end' => '24:00',
             'trip_day_id' => '1',
             'landmark_id' => '29',
            ],[
             'start' => '12:00',
             'end' => '14:00',
             'trip_day_id' => '2',
             'landmark_id' => '36',
            ],[
             'start' => '15:00',
             'end' => '16:00',
             'trip_day_id' => '2',
             'landmark_id' => '34',
            ],[
             'start' => '16:05',
             'end' => '17:00',
             'trip_day_id' => '2',
             'landmark_id' => '35',
            ],[
             'start' => '17:05',
             'end' => '19:00',
             'trip_day_id' => '2',
             'landmark_id' => '33',
            ],[
             'start' => '21:00',
             'end' => '24:00',
             'trip_day_id' => '2',
             'landmark_id' => '32',
            ],[
             'start' => '11:00',
             'end' => '12:00',
             'trip_day_id' => '3',
             'landmark_id' => '39',
            ],[
             'start' => '12:30',
             'end' => '15:00',
             'trip_day_id' => '3',
             'landmark_id' => '38',
            ],[
             'start' => '15:30',
             'end' => '18:30',
             'trip_day_id' => '3',
             'landmark_id' => '37',
            ],
        ]);
    }
}
