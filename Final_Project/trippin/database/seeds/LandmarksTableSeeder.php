<?php

use Illuminate\Database\Seeder;

class LandmarksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('landmarks')->insert([
            [
             'landmark_name' => 'Raouche Rock',
             'long' => '35.470436', 'lat' => '33.889555', 'cost' => '0.00',
             'landmark_description' => 'lorem ipsum', 'city_id' => '1', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Bethesda Fountain',
             'long' => '-73.970805', 'lat' => '40.774352', 'cost' => '0',
             'landmark_description' => 'The Bethesda Fountain is one of the largest fountains in New York. A superb place to enjoy the outdoors.', 'city_id' => '6', 'visit_purpose_id' => '3',
            ],[
             'landmark_name' => 'Strawberry Fields',
             'long' => '-73.974621', 'lat' => '40.775693', 'cost' => '0',
             'landmark_description' => 'Named after the title of the Beatles\' song "Strawberry Fields Forever", the teardrop shaped region was dedicated to John Lennon', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Bow Bridge',
             'long' => '-73.971635', 'lat' => '40.775525', 'cost' => '0',
             'landmark_description' => 'Bow Bridge is named for its graceful shape, reminiscent of the bow of an archer or violinist. This handsomely designed bridge spans the Lake, linking Cherry Hill with the woodland of the Ramble', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Time Square',
             'long' => '-73.985069', 'lat' => '40.758678', 'cost' => '60',
             'landmark_description' => 'One of the world\'s busiest pedestrian intersections, it is also the hub of the Broadway Theater District and a major center of the world\'s entertainment industry.', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Rockefeller Center',
             'long' => '-73.978785', 'lat' => '40.758522', 'cost' => '30',
             'landmark_description' => 'The famous Rockefeller Center in Midtown Manhattan is an Art Deco NYC landmark. It is one of the most popular attractions in NYC. It is home to numerous NY events', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Madison Square Garden',
             'long' => '-73.994308', 'lat' => '40.750935', 'cost' => '25',
             'landmark_description' => 'Madison Square Garden opened in 1968, and is one of the oldest active arenas in the NBA and NHL.', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Madison Square Park',
             'long' => '-73.988143', 'lat' => '40.742070', 'cost' => '25',
             'landmark_description' => 'Madison Square Park is a true urban park nestled in the heart of Flatiron and NoMad. Bask in the quiet reflection, see stunning art and gorgeous gardens.', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Flatiron Building',
             'long' => '-73.989742', 'lat' => '40.740930', 'cost' => '10',
             'landmark_description' => 'You might not know it by name but the Flatiron Building has been featured in many New York movies and T.V. series. You would have seen Flatiron in the films Reds, Godzilla and Spiderman as well as the T.V. shows Friends, Spin City and Veronica\'s Closet.', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Washington Square Park',
             'long' => '-73.997933', 'lat' => '40.730855', 'cost' => '10',
             'landmark_description' => 'A gathering spot for avant-garde artists. A battleground for chess enthusiasts. A playground for canines and children. Washington Square Park has served various roles for its community throughout the years.', 'city_id' => '6', 'visit_purpose_id' => '3',
            ],[
             'landmark_name' => 'Macy’s',
             'long' => '-73.990331', 'lat' => '40.751293', 'cost' => '100',
             'landmark_description' => 'Macy\'s on Herald Square markets itself as the "world\'s biggest store", the iconic retail department store has been around since 1902 and has accumulated a number of "firsts" in its history.', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Empire State Building',
             'long' => '-73.985428', 'lat' => '40.748416', 'cost' => '10',
             'landmark_description' => 'The Empire State Building is more than just a view. It’s an immersive experience inside a world famous landmark.', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Statue of Liberty',
             'long' => '-74.045101', 'lat' => '40.689664', 'cost' => '30',
             'landmark_description' => '"The Statue of Liberty Enlightening the World" was a gift of friendship from the people of France to the United States and is recognized as a universal symbol of freedom and democracy.', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'World Trade Center Site',
             'long' => '-74.014549', 'lat' => '40.711528', 'cost' => '50',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Wall Street',
             'long' => '-74.010989', 'lat' => '40.706004', 'cost' => '30',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Chinatown',
             'long' => '-74.001258', 'lat' => '40.716089', 'cost' => '50',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Grand Central Terminal',
             'long' => '-73.979418', 'lat' => '40.752730', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'New York Public Library',
             'long' => '-73.984442', 'lat' => '40.753186', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Bryant Park',
             'long' => '-73.985421', 'lat' => '40.753600', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '3',
            ],[
             'landmark_name' => 'St. Patrick\'s Cathedral',
             'long' => '-74.027774', 'lat' => '40.7733007', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => '5th Avenue',
             'long' => '-73.9731227', 'lat' => '40.7639952', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Century 21',
             'long' => '-74.005619', 'lat' => '40.706109', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Brooklyn Bridge',
             'long' => '-73.999053', 'lat' => '40.706089', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Brooklyn Bridge Park',
             'long' => '-73.996654', 'lat' => '40.700420', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '3',
            ],[
             'landmark_name' => 'Brooklyn Heights Promenade',
             'long' => '-74.00006', 'lat' => '40.6959365', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Verboten',
             'long' => '-73.959059', 'lat' => '40.722093', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Intrepid Sea, Air &amp; Space Museum',
             'long' => '-73.999608', 'lat' => '40.764527', 'cost' => '',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Hudson River Park',
             'long' => '-74.014690', 'lat' => '40.730859', 'cost' => '40',
             'landmark_description' => 'lorem ipsum', 'city_id' => '6', 'visit_purpose_id' => '1',
            ],[
             'landmark_name' => 'Music Hall',
             'long' => '35.494373,', 'lat' => '33.898602', 'cost' => '80.00',
             'landmark_description' => 'Music Hall is a cabaret-style musichall theatre specializing in live entertainment and cultural showbiz', 'city_id' => '1', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Beirut Souks',
             'long' => '35.503902', 'lat' => '33.899885', 'cost' => '150.00',
             'landmark_description' => 'The Beirut Souks is a mega-shopping district in Downtown Beirut. It is home to more than 200 shops and a department store, thus making it Beirut\'s biggest shopping strip.', 'city_id' => '1', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Zaitunay Bay',
             'long' => '35.498701', 'lat' => '33.902595', 'cost' => '75.00',
             'landmark_description' => 'An innovative tourist attractionthat is conceived as an urban beach. It provides outdoor spaces and public areas for displaying artwork. It also includes lounge bars and restaurants.', 'city_id' => '1', 'visit_purpose_id' => '3',
            ],[
             'landmark_name' => 'Mar Mikhael',
             'long' => '35.524605', 'lat' => '33.896654', 'cost' => '30.00',
             'landmark_description' => 'Beirut\'s bars and pubs are all centered around Mar Mikhael!', 'city_id' => '1', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Nejme Square',
             'long' => '35.504589', 'lat' => '33.896821', 'cost' => '40.00',
             'landmark_description' => 'Nejme Square, or Place de l\'Étoile, is the central square in the Downtown area. It is home to the Lebanese Parliament and its complementary buildings, two cathedrals, a museum, and several cafes and restaurants', 'city_id' => '1', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Mohammad Al-Amin Mosque',
             'long' => '35.505897', 'lat' => '33.895153', 'cost' => '0.00',
             'landmark_description' => 'The Mohammad Al-Amin Mosque is a Sunni Muslim mosque located on Beirut\'s Martyrs\' Square. It is most known for being the backdrop of the Cedar Revolution events in 2005', 'city_id' => '1', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'Saint George Maronite Cathedral',
             'long' => '35.504953', 'lat' => '33.896168', 'cost' => '0.00',
             'landmark_description' => 'The Saint George Maronite Cathedral is the cathedral of the Maronite Archdiocese in Beirut.', 'city_id' => '1', 'visit_purpose_id' => '2',
            ],[
             'landmark_name' => 'ABC Ashrafieh',
             'long' => '35.518462', 'lat' => '33.889023', 'cost' => '100.00',
             'landmark_description' => 'ABC is the most prominent mall and department store in Lebanon that offers a unique shopping, dining and cinema experience', 'city_id' => '1', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'Ein El Mreisseh',
             'long' => '35.489647', 'lat' => '33.901438', 'cost' => '10.00',
             'landmark_description' => 'Enjoy a long walk around the beach of Beirut. Make sure to rent a bike for a better ride.', 'city_id' => '1', 'visit_purpose_id' => '1',
            ],[
             'landmark_name' => 'Hamra',
             'long' => '35.479036', 'lat' => '33.896104', 'cost' => '20.00',
             'landmark_description' => 'Hamra is one of the main streets of the city of Beirut, Lebanon, and one of the main economic hubs of Beirut. It has numerous sidewalk cafes and theatres.', 'city_id' => '1', 'visit_purpose_id' => '4',
            ],[
             'landmark_name' => 'AUB',
             'long' => '35.481300', 'lat' => '33.900699', 'cost' => '0.00',
             'landmark_description' => 'American University of Beirut is the top university in Lebanon. The campus gives you a good feel of escape from the city.', 'city_id' => '1', 'visit_purpose_id' => '2',
            ],
        ]);
    }
}
