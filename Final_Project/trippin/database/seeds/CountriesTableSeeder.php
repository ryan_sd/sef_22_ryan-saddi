<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            ['country_name' => 'Lebanon'],
            ['country_name' => 'France'],
            ['country_name' => 'Australia'],
            ['country_name' => 'USA'],
            ['country_name' => 'Italy'],
        ]);
    }
}
