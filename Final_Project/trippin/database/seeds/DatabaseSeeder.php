<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VisitPurposesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(LandmarksTableSeeder::class);
        $this->call(TripsTableSeeder::class);
        $this->call(TripCharacteristicsTableSeeder::class);
        $this->call(TripDaysTableSeeder::class);
        $this->call(DayPlansTableSeeder::class);
    }
}
