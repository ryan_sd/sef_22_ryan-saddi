<?php

use Illuminate\Database\Seeder;

class TripCharacteristicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('trip_characteristics')->insert([
            [
             'trip_id' => '1',
             'visit_purpose_id' => '1',
             'percentage' => '8',
            ],[
             'trip_id' => '1',
             'visit_purpose_id' => '2',
             'percentage' => '34',
            ],[
             'trip_id' => '1',
             'visit_purpose_id' => '3',
             'percentage' => '8',
            ],[
             'trip_id' => '1',
             'visit_purpose_id' => '4',
             'percentage' => '50',
            ],[
             'trip_id' => '2',
             'visit_purpose_id' => '1',
             'percentage' => '4',
            ],[
             'trip_id' => '2',
             'visit_purpose_id' => '2',
             'percentage' => '44',
            ],[
             'trip_id' => '2',
             'visit_purpose_id' => '3',
             'percentage' => '15',
            ],[
             'trip_id' => '2',
             'visit_purpose_id' => '4',
             'percentage' => '37',
            ],[
             'trip_id' => '3',
             'visit_purpose_id' => '1',
             'percentage' => '30',
            ],[
             'trip_id' => '3',
             'visit_purpose_id' => '2',
             'percentage' => '40',
            ],[
             'trip_id' => '3',
             'visit_purpose_id' => '3',
             'percentage' => '10',
            ],[
             'trip_id' => '3',
             'visit_purpose_id' => '4',
             'percentage' => '20',
            ],[
             'trip_id' => '4',
             'visit_purpose_id' => '1',
             'percentage' => '25',
            ],[
             'trip_id' => '4',
             'visit_purpose_id' => '2',
             'percentage' => '45',
            ],[
             'trip_id' => '4',
             'visit_purpose_id' => '3',
             'percentage' => '25',
            ],[
             'trip_id' => '4',
             'visit_purpose_id' => '4',
             'percentage' => '5',
            ],
        ]);
    }
}
