<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            ['city_name' => 'Beirut', 'greetings' => 'مرحبا', 'country_id' => '1', 'thumbnail' => 'beirut.jpg', 'cover' => 'cover_lebanon.jpg'],
            ['city_name' => 'Bekaa', 'greetings' => 'مرحبا', 'country_id' => '1', 'thumbnail' => 'bekaa.jpg', 'cover' => 'cover_lebanon.jpg'],
            ['city_name' => 'North Lebanon', 'greetings' => 'مرحبا', 'country_id' => '1', 'thumbnail' => 'north_lebanon.jpg', 'cover' => 'cover_lebanon.jpg'],
            ['city_name' => 'Paris', 'greetings' => 'Bonjour', 'country_id' => '2', 'thumbnail' => 'paris.jpg', 'cover' => 'cover_france.jpg'],
            ['city_name' => 'Marseille', 'greetings' => 'Bonjour', 'country_id' => '2', 'thumbnail' => 'marseille.jpg', 'cover' => 'cover_france.jpg'],
            ['city_name' => 'New York', 'greetings' => 'Hey', 'country_id' => '4', 'thumbnail' => 'new_york.jpg', 'cover' => 'cover_usa.jpg'],
            ['city_name' => 'Las Vegas', 'greetings' => 'Hey', 'country_id' => '4', 'thumbnail' => 'las_vegas.jpg', 'cover' => 'cover_usa.jpg'],
            ['city_name' => 'Rome', 'greetings' => 'Hola', 'country_id' => '5', 'thumbnail' => 'rome.jpg', 'cover' => 'cover_italy.jpg'],
            ['city_name' => 'Venice', 'greetings' => 'Hola', 'country_id' => '5', 'thumbnail' => 'venice.jpg', 'cover' => 'cover_italy.jpg'],
        ]);
    }
}
