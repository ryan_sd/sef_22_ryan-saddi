<?php

use Illuminate\Database\Seeder;

class VisitPurposesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('visit_purposes')->insert([
            ['purpose' => 'adventure'],
            ['purpose' => 'history & culture'],
            ['purpose' => 'relax'],
            ['purpose' => 'entertainment'],
        ]);
    }
}
