<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('day_plans', function (Blueprint $table) {
            $table->time('start');
            $table->time('end');
            $table->integer('trip_day_id')->unsigned();
            $table->foreign('trip_day_id')->references('id')->on('trip_days');
            $table->integer('landmark_id')->unsigned();
            $table->foreign('landmark_id')->references('id')->on('landmarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('day_plans');
    }
}
