<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandmarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landmarks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('landmark_name', 45);
            $table->float('long', 10, 6);
            $table->float('lat' , 10, 6);
            $table->float('cost', 10, 2);
            $table->string('landmark_description');
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->integer('visit_purpose_id')->unsigned();
            $table->foreign('visit_purpose_id')->references('id')->on('visit_purposes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('landmarks');
    }
}
