<?php

class Helper {
	
	public static function array_swap(&$array, $a, $b) {
    	$temp = $array[$a];
    	$array[$a] = $array[$b];
    	$array[$b] = $temp;
  	}

  	public function permutate(&$result, $list, $size, $start = 0) {
  		if ($start == $size - 1) {
  			$result[] = $list;
  		} else {
  			for ($i = $start; $i < $size; $i++) { 
  				self::array_swap($list, $i, $start);
  				self::permutate($result, $list, $size, $start + 1);
  				self::array_swap($list, $i, $start);
  			}
  		}
  	}

  	public static function postFixMapping(&$output, $position = 5, $state = array()) {

  		if (empty($state)) {
  			$state = array(
  				array("pos" => 0, "val" => 0),
  				array("pos" => 1, "val" => 0),
  				array("pos" => 2, "val" => 0),
  				array("pos" => 3, "val" => 0),
  				array("pos" => 4, "val" => 0),
  				array("pos" => 5, "val" => 5)			
  			);
  		}

  		if ($position == 1) {
  			return $state;
  		}

  		while ($state[$position - 1]["val"] < $state[$position - 1]["pos"]) {
  			$state[$position]["val"] -= 1;
  			$state[$position - 1]["val"] += 1;

  			$output[] = $state;

  			$fix = $state;

  			for ($i = $position - 1; $i > 1; $i--) { 
  				$fix[$i]["val"] -= 1;
  				$fix[$i - 1]["val"] += 1;

  				$output[] = $fix;
  			}
  		}
  		// echo $state[$position-1]["val"] . "\n";
  		// echo $state[$position-1]["pos"] . "\n";

  		return self::postFixMapping($output, $position - 1, $state);
  	}

	public static function sampling($set, $size, $result = array()) {
		if (empty($result)) {
			$result = $set;
		}

		if ($size == 1) {
			return $result;
		}
		
		$partial = array();

		for ($i=0; $i < count($result); $i++) { 
			for ($j=0; $j < count($set); $j++) { 
				$partial[] = $result[$i] . $set[$j];
			}
		}

		return self::sampling($set, $size - 1, $partial);
	}
	
	public static function generateEquation($operands, $operators, $map) {
		$equation = array();

		foreach ($map as $position) {
			// if ($position["pos"] == 1) {
			// 	$equation[] = array_shift($operands);
			// 	$equation[] = array_shift($operands);

			// 	if ($position["val"] > 0) {
			// 		for ($i=0; $i < $position["val"]; $i++) { 
			// 			$equation[] = array_shift($operators);
			// 		}
			// 	}

			// } else {
				$equation[] = array_shift($operands);

				if ($position["val"] > 0) {
					for ($i=0; $i < $position["val"]; $i++) { 
						$equation[] = array_shift($operators);
					}
				}
			}
		

		return $equation;
	}

}

?>