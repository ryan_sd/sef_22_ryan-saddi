<?php

class Stack {
	//Class member
	protected $list;
	private $index;

	//create an empty Stack
	function __construct() {
		$list = array();
		$index = 0;
	}

	//create a Stack from an array
	static function fromArray($list) {
		$instance = new self();
		$instance->list = $list;
		$instance->index = count($list) - 1;
		return $instance;
	}

	//Add value to the top of the Stack
	function push($val) {
		//top of the Stack is the end of the array
		$this->index++;
		$this->list[$this->index] = $val;
	}

	//Remove value from top of the Stack and return it
	function pop() {
		$lastElement = $this->peek();
		$this->index--;
		return $lastElement;
	}

	//Returns the value that is on top of the Stack
	function peek() {
		$lastIndex = $this->index;
		return $this->list[$lastIndex];
	}

	function size() {
		return $this->index + 1;
	}
}

?>