<?php

class GameGenerator {
	// Class Members
	protected $bigList;
	protected $smallList;

	protected $chosenlist;
	protected $target;

	//Class Constructor
	//initialize the 2 lists of available numbers to choose from
	function __construct() {
		$this->bigList = [25, 50, 75, 100];
		$this->smallList = [
			1, 1, 2, 2, 3, 3, 4,
			4, 5, 5, 6, 6, 7, 7,
			8, 8, 9, 9, 10, 10
		];
	}

	function getSelection() {
		return $this->chosenlist;
	}

	function getTarget() {
		return $this->target;
	}

	function generateGame()	{
		$this->generateRandomList();

		$this->generateRandomTarget();
	}

	private function generateRandomTarget() {
		$this->target = rand(101, 999);
	}

	private function generateRandomList() {
		//Select 1 to 4 numbers from the first list
		$takeFromFirst = rand(1, 4);

		$firstSelection = $this->selectFromList($this->bigList, $takeFromFirst);

		//Select the remaining numbers from the second list
		$takeFromSecond = 6 - $takeFromFirst;
		$secondSelection = $this->selectFromList($this->smallList, $takeFromSecond);

		//Merge the 2 lists to get a total of 6 numbers selected
		$this->chosenlist = array_merge($firstSelection, $secondSelection);
	}

	private function selectFromList($list, $howMany) {
		$selectedList = array();

		//shuffle the given array
		shuffle($list);

		//pick the first "how many" numbers
		for ($index = 0; $index < $howMany; $index++) {
			$selectedList[] = $list[$index];
		}

		return $selectedList;
	}

	function __toString() {
		$printSelectedList = "{ ";
		for ($i=0; $i < count($this->chosenlist); $i++) { 
			$printSelectedList .= $this->chosenlist[$i];
			if ($i < count($this->chosenlist) - 1) {
				$printSelectedList .= ", ";
			}
		}
		$printSelectedList .= " }\n";
		$printSelectedTarget = "Target: {$this->target}\n";

		return $printSelectedList . $printSelectedTarget;
	}
}

?>