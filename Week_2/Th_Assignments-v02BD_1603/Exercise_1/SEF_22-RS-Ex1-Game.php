<?php
require_once 'SEF_22-RS-Ex1-GameGenerator.php';
require_once 'SEF_22-RS-Ex1-GameSolver.php';
require_once 'SEF_22-RS-Ex1-GameOutput.php';

class Game {
	//Class Member
	//indicates how many turns should be played
	protected $count;

	function __construct($times) {
		$this->count = $times;
	}

	function playGames() {
		for ($turn = 0; $turn < $this->count; $turn++) { 
			$generator = new GameGenerator();
			$generator->generateGame();
			echo "Game successfully generated...\n";
			echo "Solving Game...\n";
			$solver = new GameSolver($generator);
			$solver->solveProblem();

			$printer = new GameOutput($solver);
			$printer->printOutResult();
			echo "\n";
		}
	}
}
$userCommand = fopen ("php://stdin","r");
echo "How many games would you like me to play today?\n";
$userChoice = trim(fgets($userCommand));
if (!is_numeric($userChoice)) {
	exit("Exiting...invalid user input\n");
}
$a = new Game($userChoice);
$a->playGames();
?>