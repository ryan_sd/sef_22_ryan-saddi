<?php
require_once 'SEF_22-RS-Ex1-Stack.php';
require_once 'SEF_22-RS-Ex1-Helper.php';
require_once 'SEF_22-RS-Ex1-GameGenerator.php';

class GameSolver {
	//Class Members
	protected $gameGenerator;

	protected $smallestRange;
	protected $bestEquation;

	function __construct($gameGiven) {
		//take the given numbers from GameGenerator
		$this->gameGenerator = $gameGiven;

		//predefined value, represents an empty range at the start of GameSolver
		$this->smallestRange = INF;
	}

	private function rangeWithTarget($result, $equation, $reach) {
		$target = $this->gameGenerator->getTarget();
		$range = $target - $result;

		//update if range is smaller than current smallest range
		if (abs($range) < $this->smallestRange) {
			$reachedEq = array_slice($equation, 0, $reach);
			$this->bestEquation = implode(' ', $reachedEq);

			$this->smallestRange = $range;

			echo $this->bestEquation . " " . $range. "\n";
		}
	}


	function getGiven() {
		return $this->gameGenerator;
	}

	public function solveProblem() {
		$selection = $this->gameGenerator->getSelection();

		//get the permutation of the selection
		$permutationList = array();
		Helper::permutate($permutationList, $selection, count($selection));

		//get combination of operators
		$operatorList = Helper::sampling(['+', '-', '*', '/'], 5);

		//populate post fix mapping
		$mapping = array();
		Helper::postFixMapping($mapping);
		foreach ($permutationList as $permutation) {
			foreach ($operatorList as $operator) {
				$operatorArr = str_split($operator, 1);
				foreach ($mapping as $map) {
					$eq = Helper::generateEquation($permutation, $operatorArr, $map);
					$this->solveEq($eq);
					if ($this->smallestRange == 0) {
						return;
					}
				}
			}
		}
	}

	private function solveEq($equation) {
		$stackOp = new Stack();

		for ($i = 0; $i < count($equation); $i++) { 
			if (is_integer($equation[$i])) {
				$stackOp->push($equation[$i]);
			} else {
				$rightOperand = $stackOp->pop();
				$leftOperand = $stackOp->pop();

				//exit if division by 0 or divison or multiplicatin by 1
				if ($equation[$i] == '/'
					&& $rightOperand == 0) {
            		return -1;
       			} else if ($equation[$i] == '*' ||
                   $equation[$i] == '/') {
					if ($leftOperand == 1 ||
						$rightOperand == 1) {
						return -1;
					}
				}

				$operation = "{$leftOperand}{$equation[$i]}{$rightOperand}";

				$partialResult = eval("return ".$operation.";");

				//At no intermediate step in the process can the current running total become negative or involve a fraction
				if ($partialResult <= 0 || is_float($partialResult)) {
					return -1;
				}

				//check if partial equation (intermediate step) found a closer answer
				$this->rangeWithTarget($partialResult, $equation, $i+1);

				//exit the equation if target reached
				if ($this->smallestRange == 0) {
					return;
				}

				//push the result back in to the stack
				$stackOp->push($partialResult);
			}
		}

		//get the final value
		$final = $stackOp->pop();
		return $this->rangeWithTarget($final, $equation, count($equation));
	}



	public function __toString() {
		$printSolution = "Solution [";

		//display range to target
		if ($this->smallestRange == 0) {
			$printSolution .= "Exact";
		} else {
			$printSolution .= "Remaining: {$this->smallestRange}";
		}

		$printSolution .= "]:\n";

		//get result of equation
		$result = $this->gameGenerator->getTarget() - $this->smallestRange;
		$printSolution .= "$this->bestEquation = $result\n";

		return $printSolution;
	}
}

?>