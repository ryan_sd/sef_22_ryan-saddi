<?php
require_once 'SEF_22-RS-Ex1-GameGenerator.php';
require_once 'SEF_22-RS-Ex1-GameSolver.php';

class GameOutput {
	//Class Members
	//keeps count of every game created in 1 round
	protected static $gameCount;
	protected $gameIndex;

	protected $gameGiven;
	protected $gameSolver;

	//Class Constructor
	function __construct($gameSolver) {
		++self::$gameCount;
		//save the curent index of the game since gameCount will be unique to all GameOutput Objects
		$this->gameIndex = self::$gameCount;

		//Store the corresponding GameGenerator with its Solutions
		$this->gameGiven = $gameSolver->getGiven();
		$this->gameSolver = $gameSolver;
	}

	function printOutResult() {
		echo "Game {$this->gameIndex}:\n";
		echo $this->gameGiven;
		echo "\n";
		echo $this->gameSolver;
		echo "\n------\n";
	}

	function __toString() {
		return "This is GameOutput#" . $this->gameIndex . "\n";
	}
}

?>