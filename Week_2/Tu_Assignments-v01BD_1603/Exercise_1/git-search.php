#!/usr/bin/php -q
<?php
function createEmptyCommitObject(){
	//Every commit has the following fields: HashID, author, email, date, time and message out of other fields.
	$emptyCommit = array(
		'hash' => '',
		'author' => '',
		'email' => '',
		'date' => '',
		'message' => '' 
	);
	return $emptyCommit;
}

function checkNameStartWith($filterName, $commit) {
	return strpos($commit['author'], $filterName) === 0;
}

function filterLogs($filterOptions, $logList) {
	$filteredResult = array();
	foreach ($logList as $commit) {
		$test = TRUE;

		if (isset($filterOptions['a'])) {
			$test &= checkNameStartWith($filterOptions['a'], $commit);
		}

		if (isset($filterOptions['e'])) {
			//compare email with given email
			$test &= $commit['email'] == $filterOptions['e'];
		}

		if (isset($filterOptions['m'])) {
			//compare message with given message
			$test &= $commit['message'] == $filterOptions['m'];
		}

		if (isset($filterOptions['t'])) {
			//date format in log: lll MMM dd [...] YYYY
			//date format needed llll
			$dateFormat = "lll MMM dd";
			//time format needed
			$timeFormat = "hh:mm";
			//get only the time from complete date
			$time = substr($commit['date'], strlen($dateFormat) + 1, strlen($timeFormat));
			
			//compare it with given time
			$test &= $time == $filterOptions['t'];
		}

		if (isset($filterOptions['d'])) {
			//create a php readable date from date string
			$date=date_create($commit['date']);
			//change it to match user input date format
			$dateLogFormatted = date_format($date,"l-m-Y");

			//compare it with given date
			$test &= $dateLogFormatted == $filterOptions['d'];
		}

		if (isset($filterOptions['s'])) {
			//convert date to epoch timestmap
			$timeStamp = strtotime($commit['date']);

			//compare it with given timestamp
			$test &= $timeStamp == $filterOptions['s'];
		}

		//if test pass
		if ($test) {
			$filteredResult[] = $commit;
		}
	}
	return $filteredResult;
}

function parseLog($logHistory) {
	//divide logHistory to lines
	$logTraverse = explode("\n", $logHistory);
	//List that will eventually contain every commit log
	$logList = array();

	$commitObj = createEmptyCommitObject();
	//read every line and create a commit object. Add that object to the list.
	foreach ($logTraverse as $line) {
		//Commit object start
		if (strpos($line, 'commit ') === 0) {
			//When new commit object found, clear old data
			if ($commitObj['hash'] != '') {
				$commitObj['message'] = trim($commitObj['message']);
				$logList[] = $commitObj;
				$commitObj = createEmptyCommitObject();
			}
			$commitObj['hash'] = substr($line, strlen('commit '));
		//If line refers to author credentials
		} elseif (strpos($line, 'Author:') === 0) {
			//Credentials are of the form 'First Last <Email>'
			$authorCredentials = substr($line, strlen('Author: '));
			//Split Index
			$seperation = strpos($authorCredentials, '<');

			//get name and store it in commit object
			$name = substr($authorCredentials, 0, $seperation - 1);
			$commitObj['author'] = $name;
			//get email and store it in commit object
			$email = substr($authorCredentials, $seperation + 1, -1);
			$commitObj['email'] = $email;
		//If line refers to date of commit
		} elseif (strpos($line, 'Date') === 0) {
			$fullDate = substr($line, strlen('Date:   '));
			$commitObj['date'] = $fullDate;
		//If line refers to message of comit + ignore empty lines
		} elseif ($line != "") {
			$commitObj['message'] .= trim($line) . " ";
		}
	}

	//to add the last commitObj
	if ($commitObj['hash'] != '') {
		$logList[] = $commitObj;
	}

	return $logList;
}

function printResults($results, $filters) {
	$total = count($results);
	$filterHeader = " ";
	foreach ($filters as $key => $filter) {
		switch ($key) {
			case 'a':
				$filterHeader .= "by author name " . $filter . " ";
				break;
			case 't':
				$filterHeader .= "during " . $filter . "oclock ";
				break;
			case 'd':
				$filterHeader .= "in " . $filter . " ";
				break;
			case 'e':
				$filterHeader .= "email: " . $filter . " ";
				break;
			case 'm':
				$filterHeader .= "with message: \"" . $filter . "\" ";
				break;
			case 's':
				$filterHeader .= "in the timestamp " . $filter . " ";
				break;
			default:
				break;
		}
	}
	if ($total > 0) {
		echo "Search results" . $filterHeader . "- Total: " . $total . "\n";
		for ($i=0; $i < $total; $i++) {
			$commit = $results[$i];

			echo ($i + 1) . ":: ";
			echo $commit['hash'];
			echo " - ";
			echo $commit['author'];
			echo " - ";
			echo $commit['date'];
			echo " - \"";
			echo $commit['message'];
			echo "\"\n";
		}
	} else {
		echo "There are no results for your search" . $filterHeader . "\n";
	}
}
//The only expected user inputs 
$params = getopt('w:a:e:t:d:s:m:');

//Force the user to specify the git folder path
if (!isset($params['w'])
	|| !is_dir($params['w'])
) {
	exit("Please insert a valid directory with -w argument.\n");
}

//get the git log history
$logHistory = shell_exec("git --git-dir " . $params['w'] . " log");

//$logList is an array of Commit Objects
$logList = parseLog($logHistory);

//Remaining: Filter according to user input
$filteredLogList = filterLogs($params, $logList);

printResults($filteredLogList, $params);
?>