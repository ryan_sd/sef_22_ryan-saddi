<?php
require_once 'Database.php';
require_once 'Table.php';

/**
* DatabaseManager is a folder that contains all Database objects
*/
class DatabaseManager {
	//Class Member
	protected $name = "RSDatabase";
	protected $path;

	protected $databases;

	function __construct() {
		$this->loadDataManager();
	}

	private function loadDataManager() {
		$user = exec("whoami");
		$managerLocation = "/home/$user/Documents/$this->name";
		$this->path = $managerLocation;

		//if databaseMan doesn't exist create a folder to it
		if (!file_exists($managerLocation)) {
			mkdir($managerLocation, 0777, true);
		}

		//load all existing databases, if any
		$this->databases = $this->loadDatabases($managerLocation);
	}

	private function loadDatabases($directory) {
		return scandir($directory);
	}

	public function addDatabase($name) {
		//if database name is unique
		if (!in_array($name, $this->databases)) {
			//create a database
			$newDatabase = new Database($name);
			//add database name to my list
			$this->databases[] = $name;
			echo "\"$name\" CREATED\n";
			return $newDatabase;
		} else {
			echo "\"$name\" already exists\n";
		}
	}

	public function removeDatabase($name) {
		if (in_array($name, $this->databases)) {

			//remove folder
			exec("rm -rf $this->path/$name");

			//remove folder name from class member databases
			//find index
			$rmIndex = array_search($name, $this->databases);
			unset($this->databases[$rmIndex]);

			echo "\"$name\" DELETED\n";
		} else {
			echo "\"$name\" doesn't exist\n";
		}
	}
}

?>