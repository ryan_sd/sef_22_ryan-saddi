<?php
require_once 'Database.php';
require_once 'Table.php';
require_once 'DatabaseManager.php';

/**
* Class helps the user to play with the DbManager
*/
class Gui {
	//Class Members
	protected $dataManager;
	protected $lastTable;
	protected $lastDatabase;

	public function __construct() {
		$this->dataManager = new DatabaseManager();
	}

	public function commandMe() {
		$userCommand = fopen ("php://stdin","r");
		//read from user input + clean up trailing spaces
		$userFeed = fgets($userCommand);
		$userFeed = trim($userFeed);
		//keep reading until user types END
		while($userFeed != "END") {
			//clean up commands
			$command = $this->cleanUserInput($userFeed);

			//shows all accepted commands
			if (!isset($command[0])) {
				echo "Type HELP to see accepted commands.\n";
			} else if ($command[0] == "HELP") {
				$this->callHelp();
			//command to get a record
			} else if (count($command) == 2 && $command[0] == "GET") {
				//check if there is a flagged table
				if (!isset($this->lastTable)) {
					echo "A Table needs to be created to retrieve info.\n";
				} else {
					$this->lastTable->findTuples($command[1]);
				}
			//command to add a record
			} elseif (count($command) >= 2 && $command[0] == "ADD") {
				//check if there is a flagged table
				if (!isset($this->lastTable)) {
					echo "A Table needs to be created to add records.\n";
				} else {
					$this->lastTable->addTuple(array_splice($command, 1));
				}
			//command to create...
			} elseif (count($command) > 2) {
				if ($command[0] == "CREATE") {
					//...a database
					if ($command[1] == "DATABASE") {
						$db = $this->dataManager->addDatabase($command[2]);
						if ($db != null) {
							$this->lastDatabase = $db;
						}
					//...a table
					} elseif (count($command) > 4 
						&& $command[1] == "TABLE"
						&& $command[3] == "COLUMNS") {
						//check if there is a flagged database
						if (!isset($this->lastDatabase)) {
							echo "A Database needs to be created to create a table.\n";
						} else {
							if (!isset($command[2])) {
								echo "Cannot pass an empty database name.\n";
							} else {
								$table = $this->lastDatabase->addTable($command[2], array_splice($command, 4));
								if ($table != null) {
									$this->lastTable = $table;
								}
							}
						}
					} else {
						echo "Invalid/missing command for CREATE.\n";
					}
				//command to delete...
				} elseif ($command[0] == "DELETE") {
					//...a database
					if ($command[1] == "DATABASE") {
						$this->dataManager->removeDatabase($command[2]);

						//empty the flagged database (and table)
						$this->lastDatabase = null;
						$this->lastTable = null;
					//...a row
					} elseif ($command[1] == "ROW") {
						if (!isset($this->lastTable)) {
							echo "A Table needs to be created to delete info.\n";
						} else {
							$this->lastTable->removeTuple($command[2]);
						}
					} else {
						echo "You can only delete a DATABASE or a ROW in a table\n";
					}
				} else {
					echo "Invalid input given.\n";
				}
			} else {
				echo "Invalid input given.\n";
			}
			$userFeed = fgets($userCommand);
			$userFeed = trim($userFeed);
		}
		fclose($userCommand);
	}

	private function cleanUserInput($user) {
		$compose = explode(",", $user);
		for ($i=0; $i < count($compose); $i++) {
			$compose[$i] = str_replace("\"", '', $compose[$i]);
			$compose[$i] = str_replace("'", '', $compose[$i]);
			$compose[$i] = trim($compose[$i]);

			if (strlen($compose[$i]) == 0) {
				unset($compose[$i]);
			}
		}
		
		return $compose;
	}

	private function callHelp() {
		echo  "-- Create a Database\n"
		. "CREATE,DATABASE,\"Database Name\"\n"
		. "-- Delete a Database\n"
		. "DELETE,DATABASE,\"Database Name\"\n"
		. "-- Create a table (Number of columns is indefinite)\n"
		. "CREATE,TABLE,\"TABLENAME\",COLUMNS,\"Column1\",\"Column2\",\"Column3\"\n"
		. "-- Add a record (The table has a non-null constraint on all columns)\n"
		. "ADD,\"10\",\"Bassem\",\"Dghaidi\",\"SEF Instructor\"\n"
		. "-- Retrieve a record\n"
		. "GET,\"10\"\n"
		. "-- Delete a record\n"
		. "DELETE,ROW,\"10\"\n";
	}

}

$a = new Gui();
$a->commandMe();

?>