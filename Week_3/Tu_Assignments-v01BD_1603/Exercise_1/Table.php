<?php

/**
* A Table object has an attributes array and and array of tuples
* Table is  stored in a csv file, first line represents the attributes
* every other line represents a tuple
*/
class Table {
	protected $name;
	protected $attributes;
	protected $database;
	protected $tuples = array();
	
	function __construct($tableName, $attributes, $database) {
		//check if database exists
		if (!is_dir($database)) {
			throw new TableException("Table was given the wrong database path");
		}
		$this->database = $database;
		$this->name = $tableName;
		//if table exists
		if (file_exists($database . "/" . $tableName)) {
			//load table + populate
			$tableStore = fopen("{$database}/{$tableName}.csv", "r");
			$attributes = explode(",", fgets($tableStore));
			$this->attributes = $attributes;
			while(!feof($tableStore)) {
				$record = explode(",", fgets($tableStore));
				$tuples[] = $record;
			}
		//new table
		} else {
			$tableStore = fopen("{$database}/{$tableName}.csv", "w");
			$this->attributes = $attributes;
			$header = implode(",", $attributes);
			fwrite($tableStore, $header."\n");
			fclose($tableStore);
		}
	}

	public function addTuple($tuple) {
		if (!is_numeric($tuple[0])) {
			echo "Record's primary key must be a number.\n";
		} else {
			$primaryKey = $tuple[0];
			if (isset($this->tuples[$primaryKey])) {
				echo "Primary Key already exsits, table cannot have duplicates.\n";
			} else {
				if (count($tuple) < count($this->attributes)) {
					echo "The table has a non-null constraint on all columns,\nmake sure to fill them all up.\n";
				} elseif (count($tuple) > count($this->attributes)) {
					echo "The table was provided with more keys than it expects.\n";
				} else {
					$this->tuples[$primaryKey] = array();
					$this->tuples[$primaryKey][] = $primaryKey;
					for ($i=1; $i < count($this->attributes); $i++) { 
						$this->tuples[$primaryKey][] = $tuple[$i];
					}
					$tableStore = fopen("{$this->database}/{$this->name}.csv", "a");
					fwrite($tableStore, implode(",", $tuple)."\n");
					fclose($tableStore);
					echo "Record ADDED\n";
				}
			}
		}
	}

	public function removeTuple($index) {
		if (!is_numeric($index)) {
			echo "Please select the record's primary key in order to remove it\n";
		} else {
			if (!isset($this->tuples[$index])) {
				echo "Record $index doesnt exist.\n";
			}
			unset($this->tuples[$index]);
			$this->updateFile();
			echo "Record DELETED\n";
		}
	}

	private function updateFile() {
		$tableStore = fopen("{$this->database}/{$this->name}.csv", "w+");
		fwrite($tableStore, implode(",", $this->attributes)."\n");
		foreach ($this->tuples as $tuple) {
			fwrite($tableStore, implode(",", $tuple)."\n");
		}
		fclose($tableStore);
	}

	public function findTuples($key) {
		$found = false;
		foreach ($this->tuples as $tuple) {
			for ($i=0; $i < count($tuple); $i++) {
				if ($tuple[$i] == $key) {
					echo implode(",", $tuple)."\n";
					$found = true;
					break;
				}
			}
		}
		if (!$found) {
			echo "No tuples with the key $key exist.\n";
		}
	}
}

?>