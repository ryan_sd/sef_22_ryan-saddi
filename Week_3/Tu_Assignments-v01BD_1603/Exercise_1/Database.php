<?php
require_once 'Table.php';
/**
* Database is a folder of given name located in
* /home/user/Documents/RSDatabase/{Database Name}
*/
class Database {
	//Class Member
	protected $name;
	protected $path;

	protected $tables;

	function __construct($databaseName) {
		$this->name = $databaseName;

		//load Database, create if it doesn't exist
		$path = $this->loadDatabase($databaseName);

		//save path
		$this->path = $path;
	}

	private function loadDatabase($name) {
		$user = exec("whoami");
		//path where all databases are located
		$databaseLocation = "/home/$user/Documents/RSDatabase";

		$fullPath = "{$databaseLocation}/{$name}";

		//create folder if it doesn't exist
		if (!file_exists($fullPath)) {
			mkdir($fullPath, 0777, true);
		}

		//load existing tables, if any
		$this->tables = $this->loadTables($fullPath);
		
		return $fullPath;
	}

	private function loadTables($directory) {
		return scandir($directory);
	}

	public function addTable($name, $columns) {
		//check if table name already exist,
		if (!in_array($name.".csv", $this->tables)) {
			//create table
			$newTable = new Table($name, $columns, $this->path);
			//add name of table in my list
			$this->tables[] = $name.".csv";
			echo "\"$name\" CREATED\n";
			return $newTable;
		} else {
			echo "\"$name\" already exists\n";
			return null;
		}
	}

	public function __toString() {
		return "Database $this->name, path: $this->path";
	}
}

?>