var succShadow = "rgb(64, 209, 42) 0px 0px 20px 0px";
var warnShadow = "#ff1919 0px 0px 20px 0px";

$.ajaxPrefilter( function (options) {
  if (options.crossDomain && jQuery.support.cors) {
    var http = (window.location.protocol === 'http:' ? 'http:' : 'https:');
    options.url = http + '//cors-anywhere.herokuapp.com/' + options.url;
    //options.url = "http://cors.corsproxy.io/url=" + options.url;
  }
});

function toggleLoadingVisibility(elt) {
	if ($(elt).css('visibility') == 'hidden') {
		$(elt).css('visibility','visible');
	} else {
		$(elt).css('visibility','hidden');
	}
}

function loadArticleFromLink(link) {
	// start load animation
	toggleLoadingVisibility('#loadLink');
	// load article
	$.ajax({
		url: 'cross.php?link='+link,
		success:function(html){
			// insert the page in a container
			// to be able to read its DOM
			var container = document.createElement('DIV');
			container.innerHTML = html;
			// read main section of the page
			var article = container.getElementsByClassName('section-inner')[0];
			// get every paragraph in the article
			var articleTitle = $(article).find("h3")[0];
			var articleText = $(article).find("p");
			// clear all elements in the result box
			$("#long").empty();
			// display the title of the artice
			$("#article-title").empty();
			$("#article-title").append(articleTitle);
			// add all paragraphs to display
			for (var i = 0; i < articleText.length; i++) {
				$("#long").append(articleText[i]);
			}
			// get the box shadow back to green
     		$("#long").css("box-shadow", succShadow);
     	},
     	error:function(){
     		$("#long").empty();
     		$("#long").css("box-shadow", warnShadow);
     	},
  		complete: function () {
			// stop load animation
			toggleLoadingVisibility('#loadLink');
  		}
     });
}

function summarizeText() {
	// start load animation
	toggleLoadingVisibility('#loadSum');
	// ge the title of article
	var longTitle = $("#article-title").text();
	console.log(longTitle);
	// get the text to summarize
	var longText = $("#long").text();
	var sentencesNumber = $("#stepper").val();
	$.ajax({
		url:  'https://api.aylien.com/api/v1/summarize',
		type: "get", //send it through get method
		headers: {
	        'X-AYLIEN-TextAPI-Application-Id': 'd47e9d18',
	        'X-AYLIEN-TextAPI-Application-Key': 'ae037eb74d165477fd46c4fed27340ff'
    	},
  		data: {
  			title: longTitle,
  			text: longText,
  			sentences_number: sentencesNumber
  		},
  		success: function (json) {
  			$("#short").empty();
  			// get the new sentences
  			summarizedSentences = json.sentences;
  			// display sentences
  			for (var i = 0; i < summarizedSentences.length; i++) {
  				$("#short").append("<p>"+summarizedSentences[i]+"</p>");
  			}
			// get the box shadow back to green
     		$("#short").css("box-shadow", succShadow);
  		},
  		error:function(){
     		$("#short").empty();
     		$("#short").css("box-shadow", warnShadow);
     	},
  		complete: function () {
			// stop load animation
			toggleLoadingVisibility('#loadSum');
  		}
	});
}

function initiateSummary() {
	// if text hasn't already been selected
	if ($("#long").css("box-shadow") != succShadow) {
		$("#short").css("box-shadow", warnShadow);
	} else {
		// text is ready
		summarizeText();
	}
}

function initiateFetchArticle() {
	// regex to check if link is from medium.com
	var regexURL = /(https?:\/\/)?(www.)?medium.com\/[a-zA-Z\-.0-9]+\//;
	// get the inputed link from the user
	var link = $('#link').val();

	if (regexURL.test(link)) {
		loadArticleFromLink(link);
	} else {
		alert("FAIL");
	}
}

$(document).ready(function() {
	// on load-btn click load article from the inputed link
	$('#load').click(initiateFetchArticle);
	// on trim-btn click summarize text if available
	$('#sum').click(initiateSummary);
});


// https://medium.com/@haikutography/when-does-coming-out-end-47ae8b00ae0c#.hx5tyjhbz