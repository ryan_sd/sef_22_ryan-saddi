SELECT C.name, COUNT(*) FROM `sakila`.`category` AS C
LEFT JOIN `sakila`.`film_category` AS FC
ON C.category_id = FC.category_id
GROUP BY C.category_id
HAVING COUNT(*) BETWEEN '55' AND '65'
ORDER BY COUNT(*);