SELECT L.name as Language, COUNT(L.name) as NumberOfMovies
FROM `sakila`.`film` as F
LEFT JOIN `sakila`.`language` as L
ON L.language_id = F.language_id
WHERE F.release_year = 2006
GROUP BY L.name ASC
ORDER BY COUNT(L.name)
LIMIT 3;