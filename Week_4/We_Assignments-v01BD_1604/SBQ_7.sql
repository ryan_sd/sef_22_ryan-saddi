SELECT C.first_name, C.last_name, C.customer_id AS id
FROM `sakila`.`actor` as A
INNER JOIN `sakila`.`customer` AS C
ON A.first_name = C.first_name
WHERE A.actor_id = 8
UNION
SELECT first_name, last_name, actor_id
FROM `sakila`.`actor`;