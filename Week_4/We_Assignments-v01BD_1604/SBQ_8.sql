SELECT MONTH(P.payment_date) AS month, YEAR(P.payment_date) AS year, S.store_id, AVG(P.amount),
	   SUM(P.amount), COUNT(P.amount)
FROM `sakila`.`customer` AS C
RIGHT JOIN `sakila`.`store` AS S
ON S.store_id = C.store_id
LEFT JOIN `sakila`.`payment` AS P
ON P.customer_id = C.customer_id
GROUP BY S.store_id, MONTH(P.payment_date), YEAR(P.payment_date);