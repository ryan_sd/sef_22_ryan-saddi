SELECT A.first_name, A.last_name, F.release_year
FROM `sakila`.`actor` AS A
INNER JOIN `sakila`.`film_actor` AS FA
ON A.actor_id = FA.actor_id
INNER JOIN `sakila`.`film` AS F
ON FA.film_id = F.film_id
WHERE F.description LIKE '%crocodile%'
AND F.description LIKE '%Shark%'
ORDER BY A.last_name;