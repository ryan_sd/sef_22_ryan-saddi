SELECT CTR.country, count(CTR.country) as people
FROM `sakila`.`customer` as C
LEFT JOIN `sakila`.`address` as A
ON C.address_id = A.address_id
LEFT JOIN `sakila`.`city` as CT
ON A.city_id = CT.city_id
LEFT JOIN `sakila`.`country` as CTR
ON CT.country_id = CTR.country_id
GROUP BY CTR.country
ORDER BY COUNT(CTR.country) DESC
LIMIT 3;