SELECT concat(A.first_name, A.last_name) as Actor, COUNT(*) as NumberOfMovies
FROM `sakila`.`actor` as A
INNER JOIN `sakila`.`film_actor` as FA
ON A.actor_id = FA.actor_id
GROUP BY A.actor_id;