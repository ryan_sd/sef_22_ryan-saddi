SET @given_year = 2005;
SELECT C.first_name, C.last_name, COUNT(C.customer_id)
FROM `sakila`.`customer` AS C
LEFT JOIN `sakila`.`rental` AS R
ON C.customer_id = R.customer_id
WHERE YEAR(R.rental_date) = @given_year
GROUP BY C.customer_id
ORDER BY COUNT(C.customer_id) DESC
LIMIT 3;