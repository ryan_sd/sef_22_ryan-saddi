SELECT MC.claim_id, MC.patient_name, SC.claim_status
FROM (SELECT LS.claim_id, C.patient_name, MIN(LS.last_seq) AS min_seq
	FROM (SELECT LE.claim_id, LE.defendant_name, MAX(SC.claim_seq) as last_seq
		FROM `ClaimDB`.`LegalEvents` AS LE
		LEFT JOIN `ClaimDB`.`ClaimStatusCodes` AS SC
		ON SC.claim_status = LE.claim_status
		GROUP BY LE.claim_id, LE.defendant_name) AS LS
		LEFT JOIN `ClaimDB`.`Claims` AS C ON C.claim_id = LS.claim_id
		GROUP BY LS.claim_id) AS MC
LEFT JOIN `ClaimDB`.`ClaimStatusCodes` AS SC ON MC.min_seq = SC.claim_seq;
