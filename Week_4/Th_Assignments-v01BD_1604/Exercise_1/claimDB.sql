CREATE DATABASE  IF NOT EXISTS `ClaimDB` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ClaimDB`;
-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: ClaimDB
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ClaimStatusCodes`
--

DROP TABLE IF EXISTS `ClaimStatusCodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClaimStatusCodes` (
  `claim_status` varchar(2) NOT NULL,
  `claim_status_desc` varchar(45) NOT NULL,
  `claim_seq` int(10) unsigned NOT NULL,
  PRIMARY KEY (`claim_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ClaimStatusCodes`
--

LOCK TABLES `ClaimStatusCodes` WRITE;
/*!40000 ALTER TABLE `ClaimStatusCodes` DISABLE KEYS */;
INSERT INTO `ClaimStatusCodes` VALUES ('AP','Awaiting review panel',1),('CL','Closed',4),('OR','Panel opinior rendered',2),('SF','Suit filed',3);
/*!40000 ALTER TABLE `ClaimStatusCodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Claims`
--

DROP TABLE IF EXISTS `Claims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Claims` (
  `claim_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_name` varchar(30) NOT NULL,
  PRIMARY KEY (`claim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Claims`
--

LOCK TABLES `Claims` WRITE;
/*!40000 ALTER TABLE `Claims` DISABLE KEYS */;
INSERT INTO `Claims` VALUES (1,'Bassem Dghaidi'),(2,'Omar Breidi'),(3,'Marwan Sawwan');
/*!40000 ALTER TABLE `Claims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Defendants`
--

DROP TABLE IF EXISTS `Defendants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Defendants` (
  `claim_id` int(10) unsigned NOT NULL,
  `defendant_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Defendants`
--

LOCK TABLES `Defendants` WRITE;
/*!40000 ALTER TABLE `Defendants` DISABLE KEYS */;
INSERT INTO `Defendants` VALUES (1,'Jean Skaff'),(1,'Elie Meouchi'),(1,'Radwan Sameh'),(2,'Joseph Eid'),(2,'Paul Syoufi'),(2,'Radwan Sameh'),(3,'Issam Awwad');
/*!40000 ALTER TABLE `Defendants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LegalEvents`
--

DROP TABLE IF EXISTS `LegalEvents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LegalEvents` (
  `claim_id` int(10) unsigned NOT NULL,
  `defendant_name` varchar(30) NOT NULL,
  `claim_status` varchar(2) NOT NULL,
  `change_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LegalEvents`
--

LOCK TABLES `LegalEvents` WRITE;
/*!40000 ALTER TABLE `LegalEvents` DISABLE KEYS */;
INSERT INTO `LegalEvents` VALUES (1,'Jean Skaff','AP','2016-01-01'),(1,'Jean Skaff','OR','2016-02-02'),(1,'Jean Skaff','SF','2016-03-01'),(1,'Jean Skaff','CL','2016-04-01'),(1,'Radwan Sameh','AP','2016-01-01'),(1,'Radwan Sameh','OR','2016-02-02'),(1,'Radwan Sameh','SF','2016-03-01'),(1,'Elie Meouchi','AP','2016-01-01'),(1,'Elie Meouchi','OR','2016-02-02'),(2,'Radwan Sameh','AP','2016-01-01'),(2,'Radwan Sameh','OR','2016-02-01'),(2,'Paul Syoufi','AP','2016-01-01'),(3,'Issam Awwad','AP','2016-01-01');
/*!40000 ALTER TABLE `LegalEvents` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-07 16:03:12
