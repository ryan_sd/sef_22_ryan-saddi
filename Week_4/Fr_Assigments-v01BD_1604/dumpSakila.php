<?php
require_once 'config.php';

function printQueryResult($result) {
	$count = $result->num_rows;
	for ($i=0; $i < $count; $i++) {
		$result->data_seek($i);
		$row = $result->fetch_array(MYSQLI_ASSOC);
		var_dump($row);
	}
}

function executeQuery($query, $dbCon) {
	$result = $dbCon->query($query);

	if (!$result) {
		die($dbCon->error);
	} else {
		printQueryResult($result);
	}

	$result->close();
}

$dbCon =  new mysqli($address, $username, $password, $schema);

if ($dbCon->connect_error) {
	die($dbCon->connect_error);
} else {
	echo "Success, you are now connected to: $schema\n";

	$query = "SELECT concat(A.first_name, A.last_name) as Actor, COUNT(*) as 		NumberOfMovies
			  FROM `sakila`.`actor` as A
			  INNER JOIN `sakila`.`film_actor` as FA
			  ON A.actor_id = FA.actor_id
			  GROUP BY A.actor_id;";

	executeQuery($query, $dbCon);

	$query = "SELECT L.name as Language, COUNT(L.name) as NumberOfMovies
			  FROM `sakila`.`film` as F
			  LEFT JOIN `sakila`.`language` as L
			  ON L.language_id = F.language_id
			  WHERE F.release_year = 2006
			  GROUP BY L.name ASC
			  ORDER BY COUNT(L.name)
			  LIMIT 3;";

	executeQuery($query, $dbCon);
	
	$query = "SELECT CTR.country, count(CTR.country) as people
			  FROM `sakila`.`customer` as C
			  LEFT JOIN `sakila`.`address` as A
			  ON C.address_id = A.address_id
			  LEFT JOIN `sakila`.`city` as CT
			  ON A.city_id = CT.city_id
			  LEFT JOIN `sakila`.`country` as CTR
			  ON CT.country_id = CTR.country_id
			  GROUP BY CTR.country
			  ORDER BY COUNT(CTR.country) DESC
			  LIMIT 3;";

	executeQuery($query, $dbCon);
	
	$query = "SELECT AD.address2 FROM `sakila`.`address` AS AD
			  WHERE AD.address2 != ''
			  ORDER BY AD.address2 ASC;";

	executeQuery($query, $dbCon);
	
	$query = "SELECT A.first_name, A.last_name, F.release_year
			  FROM `sakila`.`actor` AS A
			  INNER JOIN `sakila`.`film_actor` AS FA
	  		  ON A.actor_id = FA.actor_id
			  INNER JOIN `sakila`.`film` AS F
			  ON FA.film_id = F.film_id
			  WHERE F.description LIKE '%crocodile%'
			  AND F.description LIKE '%Shark%'
			  ORDER BY A.last_name;";

	executeQuery($query, $dbCon);
	
	$query = "SELECT C.name, COUNT(*) FROM `sakila`.`category` AS C
			  LEFT JOIN `sakila`.`film_category` AS FC
			  ON C.category_id = FC.category_id
			  GROUP BY C.category_id
			  HAVING COUNT(*) BETWEEN '55' AND '65'
			  ORDER BY COUNT(*);";

	executeQuery($query, $dbCon);
	
	$query = "SELECT C.first_name, C.last_name, C.customer_id AS id
			  FROM `sakila`.`actor` as A
			  INNER JOIN `sakila`.`customer` AS C
			  ON A.first_name = C.first_name
			  WHERE A.actor_id = 8
			  UNION
			  SELECT first_name, last_name, actor_id
			  FROM `sakila`.`actor`;";

	executeQuery($query, $dbCon);
	
	$query = "SELECT MONTH(P.payment_date) AS month,
					 YEAR(P.payment_date) AS year,
					 S.store_id, AVG(P.amount),
			  		 SUM(P.amount), COUNT(P.amount)
			  FROM `sakila`.`customer` AS C
			  RIGHT JOIN `sakila`.`store` AS S
			  ON S.store_id = C.store_id
			  LEFT JOIN `sakila`.`payment` AS P
			  ON P.customer_id = C.customer_id
			  GROUP BY S.store_id,
			  		   MONTH(P.payment_date),
			  		   YEAR(P.payment_date);";

	executeQuery($query, $dbCon);

	$query = "SELECT C.first_name, C.last_name, COUNT(C.customer_id)
			  FROM `sakila`.`customer` AS C
			  LEFT JOIN `sakila`.`rental` AS R
			  ON C.customer_id = R.customer_id
			  WHERE YEAR(R.rental_date) = @given_year
			  GROUP BY C.customer_id
			  ORDER BY COUNT(C.customer_id) DESC
			  LIMIT 3;";

	$dbCon->close();
}

?>