import java.util.ArrayList;


public class PrimeNumber {

	public static ArrayList<Integer> primeList;

	public static boolean isPrime_Slow(int x) {
		for(int i = 0; i < primeList.size(); i++){
			if (x%primeList.get(i) == 0) {
				return false;
			}
		}
		primeList.add(x);
		return true;
	}

	public static boolean isPrime_Fast(int x) {
		int sqrt = (int) Math.sqrt(x);
		int i = 0;
		int j = primeList.get(i);
		while (i < primeList.size() && j <= sqrt) {
			if (x%j == 0) {
				return false;
			}
			j = primeList.get(i);
			i++;
		}
		primeList.add(x);
		return true;
	}

	public static void main(String[] args) {
		primeList = new ArrayList<Integer>();
		primeList.add(2);
		int n = 1000000;
		int i = 3;
		while (primeList.size() < n+1) {
			isPrime_Fast(i);
			i++;
		}
		System.out.println(primeList.get(n));
	}
}
