<?php
	$location = "/var/log/apache2/access.log";
	$logsFile = fopen($location, "r");

	//read file line by line until eof
	while(!feof($logsFile)) {
  		$log = fgets($logsFile);
  		$fields = explode(" ", $log);

  		if (count($fields) > 10) {
  			$ipAdress = $fields[0];
  			$date = $fields[3];
  			$method = $fields[5];
  			$file = $fields[6];
  			$protocol = $fields[7];
  			$responseCode = $fields[8];

  			//clean up date to make it readable for PHP Processor
  			$date = preg_replace('/\[/', '', $date);
  			$date = preg_replace('/:/', ' ', $date, 1);
  			$date = preg_replace('/\//', '-', $date);

  			// echo $date;

  			//create a date object
  			$dateObj = date_create($date);
  			//format date to the required output
  			$formattedDate = date_format($dateObj, 'l, F d Y : H-i-s');

  			echo $ipAdress." -- ".$formattedDate." -- ".$method." ".$file." ".$protocol." ".$responseCode."\n";
  		}
	}

	fclose($logsFile);
?>