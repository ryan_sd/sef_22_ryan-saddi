<?php
	function traverseFolder($path){
		//get all files and folders in the folder
		$tree = scandir($path);

		foreach ($tree as $type) {
			//BASE CASE: If type is file
			if(!is_dir($path."/".$type)){
				echo $type."\n";

			//Otherwise, type is folder
			} else if ($type != "." && $type != "..") {
				traverseFolder($path."/".$type);
			}
		}
	}

	$directory = $argv[2];
	
	//check if given directory is a valid directory
	if(is_dir($directory)){
		echo "Files within ".$directory.":\n";

		traverseFolder($directory);
	} else {
		//warn the user for the wrong input
		echo "\"".$directory."\" is not a valid directory, please try again with a correct path.\n";
	}
?>