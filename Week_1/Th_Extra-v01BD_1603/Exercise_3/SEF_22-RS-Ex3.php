<?php

function randomize($size) {
	$rowOfPeople = array();
	for ($index = 0; $index < $size; $index++) { 
		$rowOfPeople[$index] = rand(0, 1);
		echo translateToColor($rowOfPeople[$index])."\n";
	}
	return $rowOfPeople;
}

function updateRule($guess, $rule) {
	if ($guess == 0) {
		if ($rule == 1) {
			return 0;
		} else {
			return 1;
		}
	}
	return $rule;
}

function takeAGuess($view, $rule) {
	if ($view[0]%2 == $rule) {
		return 1;
	} else {
		return 0;
	}
}

function translateToColor($code) {
	if ($code == 0) {
		return "b";
	} else {
		return "w";
	}
}

function checkGuess($person, $guess, $group) {
	echo "Person #".($person+1)." has guessed ".translateToColor($guess);

	if($guess == $group[$person]) {
		echo ". (S)he is right\n";
	} else {
		echo ". (S)he is wrong\n";
	}
}

function whichRuleToFollow($WhatYouSee) {
	//Black is EVEN
	if($WhatYouSee[0]%2 == 0) {
		return 0;
	//Black is ODD
	} else {
		return 1;
	}
}

//returns the colors that person can see in front of him
//returns an array of size 2 [{black#}, {white#}]
function countAllInFrontOfMe($group, $stand) {
	//Start counting from $stand +1 since the person guessing can only see the persons in front of him
	$whatISee = [0, 0];
	for ($person = $stand-1; $person >= 0; $person--) {
		if($group[$person] == 0) {
			$whatISee[0]++;
		} else {
			$whatISee[1]++;
		}
	}
	return $whatISee;
}

function startGame($group) {
	//people's agreements:
	//(0) black = Black is even, (1) white = Black is odd

	$lastPerson = count($group)-1;
	$firstView = countAllInFrontOfMe($group, $lastPerson);
	$ruleIndex = whichRuleToFollow($firstView);
	checkGuess($lastPerson, $ruleIndex, $group);
	for ($person = count($group)-2; $person >= 0; $person--) {
		//What can the person see
		$view = countAllInFrontOfMe($group, $person);

		//It's not really a guess, the group teamed up against the gamemaster >:) before standing in line
		$guess = takeAGuess($view, $ruleIndex);
		$ruleIndex = updateRule($guess, $ruleIndex);
		checkGuess($person, $guess, $group);
	}
	echo "Not bad humans, you seem to be smarter than I thought :(\n";
}

$hatLineUp = array();

$userCommand = fopen ("php://stdin","r");
echo "Hello User, I am the gamemaster! Watch as the people you add to my array will lose in guessing their hat colors >:)\nType \"b\" to add a person with a black hat or \"w\" to add a person with a white hat. Type END if you are done adding more hats.\nIf you are too lazy to add people (like Bassem), just write the number of people you want in your list and we will do it for you:\n";

//read from user input + clean up trailing spaces
$userFeed = fgets($userCommand);
$userFeed = trim($userFeed);
//keep reading until user types END
while($userFeed != "END") {
	//populate array with user's black and white hats
	if ($userFeed == "b") {
		$hatLineUp[] = 0;
	} else if ($userFeed == "w") {
		$hatLineUp[] = 1;
	//populate the list randomly with a given size from Users
	} else if (preg_match('/^[0-9]+$/', $userFeed) && count($hatLineUp) == 0) {
		$hatLineUp = randomize($userFeed);
		break;
	//user has written an invalid color
	} else {
		echo $userFeed." is not a valid hat color, please type black for a black hat or white for a white hat. Type END if you dont want to add more hats.\n";
	}
	//read the next input
	$userFeed = fgets($userCommand);
	$userFeed = trim($userFeed);
}
fclose($userCommand);
//not enough data to play game
if (count($hatLineUp) <= 1) {
	echo "You need to make the challenge harder than that :|\n";
} else {
	startGame($hatLineUp);
}

?>