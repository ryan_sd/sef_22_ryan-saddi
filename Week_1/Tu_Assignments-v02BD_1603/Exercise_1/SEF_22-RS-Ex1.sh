#!/bin/bash
filename=/home/$USER/Desktop/log_dump.csv
> $filename
for file in /var/log/*.log
do
	name=$(basename "$file")
	size=$(stat -c%s "$file")
	echo $name, $size >> $filename
done
echo Logging completed in $filename