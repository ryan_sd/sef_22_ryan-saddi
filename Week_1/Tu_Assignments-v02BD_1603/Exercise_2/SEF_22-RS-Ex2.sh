#!/bin/bash

#Virtual Memory
total=$(free | sed -n 2p | awk '{print $2}')
used=$(free | sed -n 2p | awk '{print $3}')
percentage=$(($used * 100))
percentage=$(expr $percentage / $total)

#Free Disk
diskName=()
i=0
for mount in $(df | awk '{print $1}' | sed '1d')
do
	diskName[$i]=$mount
	i=$(expr $i + 1)
done

diskUsage=()
i=0
for mount in $(df | awk '{print $5}')
do
	diskUsage[$i]=$(echo $mount | sed 's/[^0-9]*//g')
	i=$(expr $i + 1)
done

alarm=0
i=0
for usage in ${diskUsage[*]}
do
	if [ $usage -ge 80 ] ; then
		echo "ALARM: Disk $diskName[$i] is at $usage%"
		alarm=1
	fi
	i=$(expr $i + 1)
done
echo $alarm
if [ $percentage -ge 80 ]; then
	echo "ALARM: Virtual Memory is at $percentage%"
elif [ $alarm -eq 0 ]; then
	echo "Everything is OK"
fi
