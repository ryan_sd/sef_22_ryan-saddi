<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ToDo</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="js/main.js"></script>
    </head>
    <body>
        <div id="container">
            <h1>SEF Todo List</h1>
            <h2>Item</h2>
            <input type="text" id="newTodo" placeholder="Write a new Task">
            <button id="addBtn">Add</button>
            <textarea placeholder="Description" id="newDesc"></textarea>
            <p id="error">All fields are required</p>
            <hr/>
            <div id="task-list">
                    <!-- INSERT TASKS HERE -->
            </div>
        </div>
    </body>
</html>
