var taskList = [];

function loadTaskList() {
	// get the stored string from the browser API
	// parse the string to object
	var list = JSON.parse(localStorage.getItem('taskList'));
	// if it's the first time we load the page
	if (list == null) {
		// return an empty list of tasks
		list = [];
	}
	return list;
}

function displayTaskList() {
	// for every stored task
	for (task in taskList){
		// get its details
		var title = taskList[task]['title'];
		var desc = taskList[task]['desc'];
		var date = taskList[task]['date'];

		// create task
		var newTaskElement = document.createElement("DIV");
		newTaskElement.className = "todo-task";
		newTaskElement.innerHTML = "<div class=\"todo\">" + 
								   "<h2>" + title + "</h2>" +
								   "<p>added: " + date + "</p>" + 
								   "<h3>" + desc + "</h3>" + 
								   "</div>";
		// create delete button
		var editNode = document.createElement("DIV");
		editNode.className = "edit-section";
		var deleteNode = document.createElement("BUTTON");
		deleteNode.className = "delete";
		deleteNode.innerHTML = "X";
		deleteNode.addEventListener("click", deleteTask);
		editNode.appendChild(deleteNode);

		// add delete button to task
		newTaskElement.appendChild(editNode);

		// get the list of tasks
		var list = document.getElementById('task-list');

		// populate list with task
		list.appendChild(newTaskElement);

	}
}

function syncTaskList() {
	// take the taskList object and change it to a string
	var jsonTaskList = JSON.stringify(taskList);
	// store it in localStorage
	localStorage.setItem('taskList', jsonTaskList);
}

function findIndexOfTask(task) {
	var list = document.getElementsByClassName('todo-task');
	for (var lookUp = 0; lookUp < list.length; lookUp++) {
		if (list[lookUp] == task) {
			return lookUp;
		}
	}
	return -1;
}

function deleteTask() {
	//allow the user to click on delete only ONCE
	this.disabled = true;

	// this element contains the whole selected task
	var taskContainer = this.parentNode.parentNode;

	// remove task from local array
	// find index in array
	var index = findIndexOfTask(taskContainer);
	// if task was found
	if (index > -1) {
		// remove that element from the list
		taskList.splice(index, 1);
	}
	// sync with browser API
	syncTaskList();

	// display change on the page
	var repeat = setInterval(fadeOut, 30);
    var opac = 1;
    function fadeOut() {
        if (opac < 0.0) {
			taskContainer.parentNode.removeChild(taskContainer);
			clearInterval(repeat);
        } else {
            opac -= 0.1; 
            taskContainer.style.opacity = opac;
        }
    }
}

function addTask() {
	// retrieve the task's details
	var taskTitle = document.getElementById('newTodo');
	var taskDesc = document.getElementById('newDesc');

	// get rid of any empty line from the description box
	taskDesc.value = taskDesc.value.replace(/\r?\n/g, ' ');

	//get today's date
	var date = new Date();
	var options = {
        weekday: "long",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit"
    };
    var currentDate = date.toLocaleTimeString("en-us", options);

	var newTask = {
		title: taskTitle.value,
		desc: taskDesc.value,
		date: currentDate
	};

	// check if any detail is missing
	var error = document.getElementById('error');
	if (newTask.title == '' || newTask.desc == '') {
		//show error message
		var repeat = setInterval(show, 20);
	    var currHeight = 0;

	    function show() {
	        if (currHeight > 22) {
				clearInterval(repeat);
	        } else {
	            currHeight += 2; 
	            error.style.height = currHeight + "px";
	            error.style.visibility = "visible";
        	}
    	}
	} else {
		// save the new task to the taskList
		taskList.unshift(newTask);
		// sync the new change with the data storage.
		syncTaskList();

		// remove error message
		if (error.style.height >= "24px") {
			var repeat = setInterval(hide, 25);
		    var currHeight = 24;
		    function hide() {
		        if (currHeight < 0) {
		        	error.style.visibility = "hidden";
					clearInterval(repeat);
		        } else {
		            currHeight -= 2; 
		            error.style.height = currHeight + "px";
	        	}
	    	}
    	}

    	// create task
		var newTaskElement = document.createElement("DIV");
		newTaskElement.className = "todo-task";
		newTaskElement.innerHTML = "<div class=\"todo\">" + 
								   "<h2>" + newTask.title + "</h2>" +
								   "<p>added: " + newTask.date + "</p>" + 
								   "<h3>" + newTask.desc + "</h3>" + 
								   "</div>";
		// create delete button
		var editNode = document.createElement("DIV");
		editNode.className = "edit-section";
		var deleteNode = document.createElement("BUTTON");
		deleteNode.className = "delete";
		deleteNode.innerHTML = "X";
		deleteNode.addEventListener("click", deleteTask);
		editNode.appendChild(deleteNode);

		newTaskElement.appendChild(editNode);

		var list = document.getElementById('task-list');


		// display the newTaskElement on the page
		list.insertBefore(newTaskElement, list.childNodes[0]);

		// empty the details that were inputed from the user
		taskTitle.value = "";
		taskDesc.value = "";
	}

}

window.onload = function() {
	// load all tasks
	taskList = loadTaskList();
	// display them on the page
	displayTaskList();
	// get event listeners to all delete buttons
	var deleteBtn = document.getElementsByClassName('delete');
	for(var i = 0; i < deleteBtn.length; i++) {
		deleteBtn[i].addEventListener("click", deleteTask);
	}
	// allow adding new task
	var addBtn = document.getElementById('addBtn');
	addBtn.addEventListener("click", addTask);
};