function isDescendingOrder(towers) {
	return peek(towers[0]) > peek(towers[1]) &&
		   peek(towers[1]) > peek(towers[2]);
}

function isAscendingOrder(towers) {
	return peek(towers[0]) < peek(towers[1]) &&
		   (peek(towers[1]) < peek(towers[2]));
}

function updatePrevious(newPos) {
	var target = previous.indexOf(newPos);
	if (target >= 0) {
		previous[target] = target;
	}
}

function isEmpty(array) {
	return array.length == 0;
}

function peek(array) {
	if (!isEmpty(array)) {
		return array[array.length-1];
	} else {
		return 0;
	}
}

function peekTowers() {
	topPeeks = [];
	//get a peek on every tower stack
	for (var i = 0; i < towers.length; i++) {
		//populate result
		topPeeks.push(peek(towers[i]));
	}
	return topPeeks;
}

function print2DArray(array2d) {
	steps++;
	for(var i = 0; i < towers.length; i++) {
		console.log(towers[i].toString());
		document.write("<div style=\"background-color:lightgray; " +
						"width:" + (50*diskNumber) + "px;" +
						" height:" + (10*diskNumber) + "px;" +
						"float:left; margin:10px;\">");
		var color = "red";
		for (var disk = towers[i].length-1; disk >= 0; disk--) {
			document.write("<div style=\"background-color:" + color + ";" +
							" height:10px; width:"+(towers[i][disk]*50)+"px;" +
							" margin:0 auto;\">" + towers[i][disk] + "</div>");
			if (color == "red") {
				color = "cyan";
			} else {
				color = "red";
			}
		}
		document.write("</div>");
	}
	document.write("<h2 style=\"clear:left;\">" + (steps-1) + "</h2>");
	console.log("^^^^^" + (steps-1));
}

function arrayEqual(arr1, arr2) {
	if (arr2.length != arr1.length) {
		return false;
	}

	for (var i = 0; i < arr1.length; i++) {
		if (arr1[i] != arr2[i]) {
			return false;
		}
	}
	return true;
}
// used to check if game is solved,
//the game is solved when the 3rd tower is full
function towerFull(tower) {
	return tower.length == diskNumber;
}

function startMoves() {
	var firstTake = 0;
	if (diskNumber % 2 == 0) {
		firstTake = 1;
	} else {
		firstTake = 2;
	}
	towers[firstTake].push(towers[0].pop());
	previous[firstTake] = 0;

	print2DArray(towers);
}

function shiftRight() {
// while there are more moves to shift right and game didn't end
	while (!arrayEqual(peekTowers(),prevStatus) && !towerFull(towers[2])) {
		for (var curr = 0; curr < towers.length; curr++) {
			// get the top disk from current tower
			var topDisk = peek(towers[curr]);
			// the next tower
			var towerDump = curr+1;
			// while current tower is not empty and the next tower exists
			while (!isEmpty(towers[curr]) && towerDump < towers.length) {
				// save the top disk we selected
				var currDisk = topDisk;
				while (topDisk == currDisk && towerDump < towers.length) {
					// next tower's top disk
					nextTowerTop = peek(towers[towerDump]);
					// save the status of the top disks (helpful for outermost while)
					prevStatus = peekTowers();
					// if next tower is empty or 
					// topDisk could be placed in the next tower
					if (isEmpty(towers[towerDump])
					   || ( currDisk < nextTowerTop
					   		&& nextTowerTop%2 != currDisk%2
					   		&& previous[curr] != towerDump)) {
						// remove the top disk
						movedDisk = towers[curr].pop();
						// and place it in next tower
						towers[towerDump].push(movedDisk);
						// save previous state
						previous[towerDump] = curr;
						// update previous states
						updatePrevious(towerDump);
						previous[curr] = curr;
						//reset tower focus
						towerDump = curr+1;
						// take a new disk
						currDisk = peek(towers[curr]);

						print2DArray(towers);
					} else {
						//move to the next tower
						towerDump++;
					}
				}
				// update the new selection
				topDisk = currDisk;
			}
		}
	}
}

function shiftLeft() {
// while there are more moves to shift left and game didn't end
	while (!arrayEqual(peekTowers(),prevStatus) && !towerFull(towers[2])) {
		for (var curr = 0; curr < towers.length; curr++) {
			// get the top disk from current tower
			var topDisk = peek(towers[curr]);
			// the next tower
			var towerDump = curr-1;
			
			// while current tower is not empty and the next tower exists
			while (!isEmpty(towers[curr]) && towerDump >= 0) {
				// save the top disk we selected
				var currDisk = topDisk;
				while (topDisk == currDisk && towerDump >= 0) {
					// next tower's top disk
					nextTowerTop = peek(towers[towerDump]);
					// save the status of the top disks (helpful for outermost while)
					prevStatus = peekTowers();
					// if next tower is empty or 
					// topDisk could be placed in the next tower
					if (isEmpty(towers[towerDump])
					   || ( currDisk < nextTowerTop 
					   		&& nextTowerTop%2 != currDisk%2
					   		&& previous[curr] != towerDump)) {
						// remove the top disk
						movedDisk = towers[curr].pop();
						// and place it in next tower
						towers[towerDump].push(movedDisk);
						// save previous state
						previous[towerDump] = curr;
						// update previous states
						updatePrevious(towerDump);
						previous[curr] = curr;
						//reset tower focus
						towerDump = curr-1;
						//take a new disk
						currDisk = peek(towers[curr]);

						print2DArray(towers);
					} else {
						// move to the next tower
						towerDump--;
					}
				}
				// update the new selection
				topDisk = currDisk;
			}
		}
	}
}

//number of disks to play with
var diskNumber = 11;

//game consists of 3 towers (poles)
var towers = [[], [], []];

//previous save where the top disk belongs
//before its curent location in the current tower
var previous = [0, 1, 2];

// counts the steps needed to solve the game
var steps = 0;

//populate the towers with the initial state
//all disks begin in the first tower
for (var i = diskNumber; i > 0; i--) {
	towers[0].push(i);
}

//print initial state
print2DArray(towers);

//initiate first move and status
startMoves();
var prevStatus = [1, 0, 0];

// boolean to allow right-left shifts alernatively
var alternate = true;

//keep alternating until last tower is complete
while (!towerFull(towers[2])) {
	if (alternate) {
		shiftRight();
	} else {
		shiftLeft();
	}
	alternate = !alternate;
	//reset previous status when doing new shift
	prevStatus = [0, 0, 0];
}