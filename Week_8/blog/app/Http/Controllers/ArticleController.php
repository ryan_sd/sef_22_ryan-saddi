<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Auth;
use App\Http\Requests;

class ArticleController extends Controller
{
   /**
     * Show a list of all articles.
     *
     * @return Response
     */
    public function index()
    {
        $articles = Article::orderBy('created_at', 'DESC')
        				->paginate(9);
        return view('home', ['articles' => $articles]);
    }

    /**
     * displays the article of id $num
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getArticle($num)
    {
    	$article = Article::where('id', $num)->first();

    	// if the given id of the article doesn't exist
    	if ($article == null) {
    		// redirect user to home page
			return redirect('/home')
				->with('alert', "Article $num was not found");
    	}
    	// else display the article
    	return view('article', ['article' => $article]);
    }

    /**
     * Saves new article in DB
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
    	if (Auth::check()) {
	    	// The user is logged in...
	    	$user = Auth::user();
	        // Validate the request...
	        $this->validate($request, [
		        'articleTitle' => 'required|min:5|max:255',
		        'articleText' => 'required|min:130',
	    	]);
	    	
	        $article = new Article;

	        $article->title = $request->articleTitle;
			$article->text = $request->articleText;
			$article->user = $user->name;

	        $article->save();
	        // redirect user to homepage
	        return $this->index();
    	} else {
    		return redirect('/auth/login');
    	}
    }
}