window.addEvent('domready', function() {
	$('textEdit').mooEditable({
		actions: 'bold italic underline strikethrough | formatBlock justifyleft justifyright justifycenter justifyfull | insertunorderedlist insertorderedlist indent outdent | undo redo | createlink unlink | toggleview'
    });
    // Post submit
    $('theForm').addEvent('submit', function(e) {
        alert($('textEdit').value);
        return true;
    });
});
