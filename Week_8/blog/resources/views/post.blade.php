<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bloggit</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/libs/mooEdit/Assets/MooEditable.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/libs/mooEdit/Assets/MooEditable.Extras.css') }}">
        <script type="text/javascript" src="{{ URL::asset('assets/libs/mooEdit/Assets/mootools.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/libs/mooEdit/Source/MooEditable/MooEditable.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/libs/mooEdit/Source/MooEditable/MooEditable.UI.MenuList.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/libs/mooEdit/Source/MooEditable/MooEditable.Extras.js') }}"></script>

        <script src="{{ URL::asset('assets/js/main.js') }}"></script>

    </head>
    <body>
    	<div id="header">
	    	<a href="{{ URL::to('/') }}"><h2 id="logo">BLOGG<span class="green">IT.</span></h2></a>
        @if (Auth::user())
            <a href="auth/logout" class="header-nav-btn"><h3>Sign o<span class="green">ut</span></h3></a>
            <a id="logged-in"><h3>Hey, <span class="green">{{ Auth::user()->name }}</span></h3></a>
        @else
            <a href="auth/register" class="header-nav-btn"><h3>Sign<span class="green">up</span></h3></a>
            <a href="auth/login" class="header-nav-btn"><h3>Sign<span class="green">in</span></h3></a>
        @endif
    	</div>
    	<hr>
        @if (count($errors) > 0)
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="alert">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <div id="container">
            <div id="single">
            	{{ Form::open(array('url' => '/home')) }}
        			{{ Form::text('articleTitle', null, array('id' => 'input-title', 'placeholder' => 'Title...')) }}
        			{{ Form::textarea('articleText', null, array('placeholder' => 'Story...', 'id' => 'textEdit')) }}
        			{{ Form::submit('Share it') }}
    			{{ Form::close() }}
            </div>
        </div>
    </body>
</html>