<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bloggit</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css') }}">
    </head>
    <body>
    	<div id="header">
	    	<a href="{{ URL::to('/') }}"><h2 id="logo">BLOGG<span class="green">IT.</span></h2></a>
            <a href="post" class="header-nav-btn"><h3>Write your sto<span class="green">ry</span></h3></a>
        @if (Auth::user())
            <a href="auth/logout" class="header-nav-btn"><h3>Sign o<span class="green">ut</span></h3></a>
            <a id="logged-in"><h3>Hey, <span class="green">{{ Auth::user()->name }}</span></h3></a>
        @else
            <a href="auth/register" class="header-nav-btn"><h3>Sign<span class="green">up</span></h3></a>
            <a href="auth/login" class="header-nav-btn"><h3>Sign<span class="green">in</span></h3></a>
        @endif
    	</div>
    	<hr>
        <div id="container">
            <p id="alert">{{ Session::get('alert') }}</p>
            @if (count($articles) > 0)
                @for ($i = 0; $i < count($articles); $i++)
                    <a href="./article/{{ $articles[$i]->id }}">
                    	<div class="article">
                    		<h3 class="article-title">{{ $articles[$i]->title }}</h3>
                            <p class="article-author">{{ $articles[$i]->user }}</p>
                    		<p>{!! substr($articles[$i]->text, 0, 100) !!}...</p>
                            <p class="article-date">{{ $articles[$i]->created_at->format('d/m/Y H:i') }}</p>
                    	</div>
                    </a>
                @endfor
                <div>{!! $articles->render() !!}</div>
            @else
                <h3 id="empty">Be the first to write a sto<span class="green">ry!</span></h3>
            @endif
        </div>
    </body>
</html>
