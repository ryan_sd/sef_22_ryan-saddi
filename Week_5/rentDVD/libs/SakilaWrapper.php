<?php

require_once "Customer.php";
require_once "Film.php";
require_once "MySQLWrap.php";
require_once 'Config.php';

/**
 * 
 */
class SakilaWrapper extends MySQLWrap {

  /**
   * [__construct description]
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * [__destruct description]
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * [selectCustomer description]
   *
   * @return [type] [description]
   */
  public function selectCustomers() {
    $query = "SELECT " .
             "customer_id, " .
             "CONCAT(first_name, ' ', last_name) AS customer_name, " .
             "active " .
             "FROM customer " .
             "WHERE active = 1 " .
             "LIMIT 1 ";
    // Execute the query
    $queryResult = $this->dbCon->query($query);
    $customersArray = array();
    // Fetch the results
    if ($queryResult) {
      while ($row = $queryResult->fetch_assoc()) {
        // Create a new Customer Object
        $tmpCustomer = new Customer($row["customer_id"],
                                    $row["customer_name"],
                                    $row["active"]);
        // CustomersArray
        array_push($customersArray, $tmpCustomer);
      }
    }
    // Close the result
    $queryResult->close();
    // Return the array of Customers
    return $customersArray;
  }


  /**
   * Search for movies in our DB based on a 
   * param provided by the user
   *
   * @param [type] $needle [description]
   *
   * @return [type] [description]
   */
  public function searchMovieTitle($needle, $offset) {
    if (empty($needle))
      return array();
    $offset = ($offset-1)*10;
    $needle = "%{$needle}%";
    // Build the Query
    $queryStatement = $this->dbCon->prepare(
             "SELECT " .
             "F.film_id," .
             "F.title," .
             "F.description," .
             "L.name AS language_name," .
             "F.rental_rate," .
             "F.rating," .
             "COUNT(INV.store_id) AS inventory_count " .
             "FROM " .
             "film AS F " .
             "INNER JOIN " .
             "language AS L ON F.language_id = L.language_id " .
             "INNER JOIN " .
             "inventory AS INV ON INV.film_id = F.film_id " .
             "WHERE " .
             "title LIKE ? " .
             "GROUP BY INV.film_id " .
             "LIMIT ?, 10");
    // Bind the needle
    $queryStatement->bind_param('si', $needle, $offset);
    // Execute the query
    $queryStatement->execute();
    // New film object
    $queryStatement->bind_result($film_id, 
                                 $title, 
                                 $description, 
                                 $language_name, 
                                 $rental_rate, 
                                 $rating, 
                                 $inventory_count);
    // // Films Array
     $filmArray = array();
    // Loop over the results
    while($queryStatement->fetch()) {
      // Push the film into the array
      array_push($filmArray, 
                 new Film($film_id, 
                          $title, 
                          $description, 
                          $language_name, 
                          $rental_rate, 
                          $rating, 
                          $inventory_count));
    }
    // Close the stream
    $queryStatement->close();
    // Return the film array
    return $filmArray;
  }


  /**
   * [getMovieCount description]
   *
   * @param [type] $needle [description]
   *
   * @return [type] [description]
   */
  public function getMovieCount($needle) {
    if (empty($needle))
      return 0;

    $needle = "%{$needle}%";
    // Build the Query
    $queryStatement = $this->dbCon->prepare(
             "SELECT " .
             "COUNT(DISTINCT INV.film_id) as TotalFilms " .
             "FROM " .
             "inventory INV " .
             "INNER JOIN " .
             "film F ON F.film_id = INV.film_id " .
             "WHERE " .
             "F.title LIKE ? ");
    // Bind the needle
    $queryStatement->bind_param('s', $needle);
    // Execute the query
    $queryStatement->execute();
    // New film object
    $queryStatement->bind_result($TotalFilms);
    // Fetch the result
    $queryStatement->fetch();
    // Close the stream
    $queryStatement->close();
    // Return the total number of films
    return $TotalFilms;
  }
}