<?php

class Film {
	/**
	 * [$Id description]
	 *
	 * @var [type]
	 */
	public $Id;
	/**
	 * [$Title description]
	 *
	 * @var [type]
	 */
	public $Title;
	/**
	 * [$Description description]
	 *
	 * @var [type]
	 */
	public $Description;
	/**
	 * [$LanguageName description]
	 *
	 * @var [type]
	 */
	public $LanguageName;
	/**
	 * [$rentalRate description]
	 *
	 * @var [type]
	 */
	public $rentalRate;
	/**
	 * [$Rating description]
	 *
	 * @var [type]
	 */
	public $Rating;
	/**
	 * [$inventoryCount description]
	 *
	 * @var [type]
	 */
	public $inventoryCount;

	/**
	 * [__construct description]
	 *
	 * @param [type] $id          [description]
	 * @param [type] $title       [description]
	 * @param [type] $description [description]
	 * @param [type] $lang        [description]
	 * @param [type] $rentrate    [description]
	 * @param [type] $rating      [description]
	 * @param [type] $inventCount [description]
	 */
	public function __construct($id, 
	                            $title, 
	                            $description, 
	                            $lang, 
	                            $rentrate, 
	                            $rating,
	                            $inventCount) {
		$this->Id = $id;
		$this->Title = $title;
		$this->Description = $description;
		$this->LanguageName = $lang;
		$this->rentalRate = $rentrate;
		$this->Rating = $rating;
		$this->inventoryCount = $inventCount;
	}
}