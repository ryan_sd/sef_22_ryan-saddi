<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	if (Auth::check()) {
		return redirect('/profile');
	}
    return redirect('/register');
});

Route::get('/profile', 'ProfileController@index');

Route::get('/profile/{num}', 'ProfileController@viewUser')
	->where('num', '[0-9]+');

Route::get('/post/{num}', 'PostController@getPost')
	->where('num', '[0-9]+');

Route::post('/post/{num}/like', 'PostController@likePost')
	->where('num', '[0-9]+');

Route::get('/post/{num}/comments', 'CommentController@getComments')
	->where('num', '[0-9]+');

Route::post('/post/{num}/comments', 'CommentController@sendComment')
	->where('num', '[0-9]+');

Route::get('/upload', function () {
	return view('upload');
});

Route::get('/wall', 'WallController@getPosts');

Route::post('/upload', 'UploadPhotoController@store');

Route::auth();