<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class UploadPhotoController extends Controller
{
	public function store(Request $request) {
		if (Auth::check()) {

			$this->validate($request, [
		        'image' => 'required|max:500|mimes:png,jpg,jpeg'
	    	]);
			// get image extension
			$extension = $request->file('image')
							->getClientOriginalExtension();
			// get timestamp
			$date = date_create();
			$currentTime = date_timestamp_get($date);
			// get user id
			$userID = Auth::user()->id;
			// image store name
			$imageName =  "{$userID}_{$currentTime}.{$extension}";
			// path
			$folder = config('app.store_image_folder');
			$path = "{$folder}/{$userID}";
			// store image on disk
		    $request->file('image')->move(
		        base_path() . '/public/' . $path, $imageName
		    );

		    $post = new Post;

			$post->url = $path . '/' . $imageName;
			$post->user_id = $userID;

	        $post->save();
		    return redirect('/profile'); 
		} else {
			return redirect('/');
		}  
	}
}
