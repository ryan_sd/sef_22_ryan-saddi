<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Http\Requests;
use Auth;

class CommentController extends Controller
{
    public function getComments($num)
    {
    	$comments = Comment::wherePostId($num)->orderBy('created_at', 'DESC')->paginate(5);
        return view('comments', ['comments' => $comments]);
    }

    public function sendComment($num, Request $request)
    {
    	if (Auth::check()) {
    		$this->validate($request, [
		        'comment' => 'required|max:500'
	    	]);
    		$comment = new Comment;
            $comment->post_id = $num;
            $comment->user_id = Auth::user()->id;
            $comment->content = $request->comment;
            if ($comment->save()) {
            	return 1;
            } else {
            	return 0;
            }
    	} else {
    		return redirect('/');
    	}
    }
}
