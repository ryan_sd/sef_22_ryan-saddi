<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Post;

class WallController extends Controller
{
    public function getPosts()
    {
    	if (Auth::check()) {
    		$posts = Post::orderBy('created_at', 'DESC')
        				->paginate(9);
        	return view('wall', ['posts' => $posts]);
    	}
    	return redirect('/');
    }
}
