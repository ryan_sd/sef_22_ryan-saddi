<?php

namespace App\Http\Controllers;
use App\Post;
use App\Like;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;

class PostController extends Controller
{
    public function likePost($num)
    {
        if (Auth::check()) {
            $like = new Like;
            $like->post_id = $num;
            $like->user_id = Auth::user()->id;
            if($like->save()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return redirect('/');
        }
    }

    public function getPost($num)
    {
        if (Auth::check()) {
        	$post = Post::find($num);
        	// if the given id of the article doesn't exist
        	if ($post == null) {
        		// redirect user to home page
    			return redirect(url()->previous())
    				->with('alert', "Post $num was not found");
        	}
            // get all comments
            $comments = Post::find($num)->comments;
            // get all likes
            $likesCount = Post::find($num)->likes->count();
            // // check if the user already liked the picture
            $liked = Like::whereUserId(Auth::id())->wherePostId($num)->exists();
        	// else display the article
        	return view('post', [
                                 'post' => $post,
                                 'comments' => $comments,
                                 'likes' => $likesCount,
                                 'liked' => $liked,
                                 ]
            );
        } else {
            return redirect('/');
        }
    }
}
