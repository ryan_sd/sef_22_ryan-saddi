<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use App\Http\Requests;

class ProfileController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
    		$posts = Post::whereUserId(Auth::id())->orderBy('created_at', 'DESC')
        				->paginate(9);
        	return view('profile', ['posts' => $posts]);
    	}
    	return redirect('/');
    }

    public function viewUser($num)
    {
    	if (Auth::check()) {
    		$posts = Post::whereUserId($num)->orderBy('created_at', 'DESC')
        				->paginate(9);
        	return view('profile', ['posts' => $posts]);
    	}
    	return redirect('/');
    }
}
