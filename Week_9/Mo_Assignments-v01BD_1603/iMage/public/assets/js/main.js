window.onload = function () {
	$("#file-upload").on('change', function () {
	    var reader = new FileReader();

	    reader.onload = function (e) {
	        // get loaded data and render thumbnail.
	        document.getElementById("placeholder-image").src = e.target.result;
	    };

	    // read the image file as a data URL.
	    reader.readAsDataURL(this.files[0]);
	});
	$('#selected-post').on('click', likePost);
	$('#add-comment').on('click', sendComment);
};

function likePost() {
	$('#selected-post').addClass("liked");
	$.ajax({
		type: "POST",
		url : window.location.href + "/like",
        success: function (step) {
            var count = parseInt($("#likes-count").html()) + parseInt(step);
            $("#likes-count").html(count);
        },
        beforeSend: function(xhr){
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
        }
    });
}

function sendComment() {
	$comment = $('#user-comment').val();
	$.ajax({
		type: "POST",
		url : "./comments",
		data: { 
	        'comment': $comment,
    	},
        success: function (step) {
        	('#comments-list').append('<li>' + step + '</li>');
        },
        beforeSend: function(xhr){
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
        }
    });
}