@extends('layouts.nav')

@section('content')
<div class="container" id="post-image">
{!! Form::open(
    array(
        'url' => '/upload',
        'class' => 'form',
        'files' => true))
!!}
    @if (count($errors) > 0)
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="alert">{{ $error }}</li>
                @endforeach
            </ul>
    @endif
    <div>
        {!! Form::label('Post Your Image') !!}
        {!! Form::file('image', array(
                                'accept' => 'image/png,image/jpeg',
                                'id' => 'file-upload',
                                'class' => 'btn btn-info',
                                'required'
                                )
                        )
         !!}
        <img id="placeholder-image" class="img-thumbnail img-responsive" src="#" alt="" />
    </div>

    <div class="form-group">
        {!! Form::submit('Post it!') !!}
    </div>
{!! Form::close() !!}
</div>
@endsection