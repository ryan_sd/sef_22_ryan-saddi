@extends('layouts.nav')

@section('content')
<div id="comments-section">
    <fieldset class="form-group">
        <label for="user-comment">Add a comment...</label>
        <textarea class="form-control" id="user-comment" rows="3"></textarea>
    </fieldset>
    <button id="add-comment" class="btn btn-primary right-float" >Submit</button>
    <ul id="comments-list">
    @foreach ($comments as $comment)
        <li>
            <blockquote class="left-align">
                {{ $comment->content }}
                <footer>{{ '@' . $comment->user->name . ', ' . $comment->created_at->format('d/m/Y H:i')}}</footer>
            </blockquote>
        </li>
    @endforeach

        <div>{!! $comments->render() !!}</div>
    </ul>
</div>
@endsection