@extends('layouts.nav')

@section('content')
<div class="container center-container">
    <img id="selected-post" class="post-content {{ $liked > 0 ? 'liked':'' }}" src="{{ URL::asset($post->url) }}">
    <h3><span id="likes-count">{{ $likes }}</span> LIKES</h3>
    <h3><a href="{{ Request::url() . '/comments' }}">COMMENTS</a></h3>
</div>
@endsection