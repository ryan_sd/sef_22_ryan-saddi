@extends('layouts.nav')

@section('content')
<div id="profile" class="container">
    <div class="profile-header row">
        <div class="col-md-4 col-sm-12 text-center">
            <img alt="" id="profile-picture">
        </div>
        <div class="col-md-8 col-sm-12 profile-info">
            <div class="header-fullname">{{ '@' . $posts[0]->user->name }}</div>
            <div class="header-information">
                <!-- {{ Auth::user()->bio }} -->
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.
            </div>
        </div>
    </div>
</div>
<hr>
<div class="posts">
    @foreach ($posts as $post)
    <div class="col-sm-4 col-md-4 col-lg-4 post">
        <a href="{{ URL::to('/post/'.$post->id) }}">
        <div class="post-content" style="background-image: url({{ URL::asset($post->url) }}">
        </div>
        </a>
    </div>
    @endforeach
    <div>{!! $posts->render() !!}</div>
</div>
@endsection