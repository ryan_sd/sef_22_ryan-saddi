@extends('layouts.nav')

@section('content')
<div class="container center-container">
@foreach ($posts as $post)
    <a href="{{ URL::to('/post/'.$post->id) }}">
	    <img class="post-content" src="{{ URL::asset($post->url) }}"></a>
	    <a href="{{ URL::to('/profile/'.$post->user->id) }}"><h4>{{ '@' . $post->user->name }}</h4></a>
    <hr/>
@endforeach
</div>
@endsection