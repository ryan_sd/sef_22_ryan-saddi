<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />
    
    <title>Laravel</title>
    
    <script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>    
    <script type="text/javascript" src="{{ URL::asset('assets/js/main.js') }}"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link href="{{ URL::asset('assets/css/main.css') }}" rel="stylesheet">
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
                <h3 id="logo"><a href="{{  URL::to('/') }}">iMage<img src="{{ URL::asset('assets/img/logo.png') }}" height=30px></a></h3>
        </div>
    </nav>
    @yield('content')
    @if (Auth::check())
    <div id="navigation-footer" class="row">
        <nav class="col-sm-8 col-sm-offset-2">
            <a href="{{  URL::to('/wall') }}" class="btn btn-success">
                WALL<br/>
                <img id="wall" src="{{ URL::asset('assets/img/nav_sprites.png') }}">
            </a>
            <a href="{{  URL::to('/profile') }}" class="btn btn-warning">
                PROFILE<br/>
                <img id="profile" src="{{ URL::asset('assets/img/nav_sprites.png') }}">
            </a>
            <a href="{{  URL::to('/upload') }}" class="btn btn-info">
                UPLOAD<br/>
                <img id="upload" src="{{ URL::asset('assets/img/nav_sprites.png') }}">
            </a>
           <a href="{{  URL::to('/logout') }}" class="btn btn-danger">
                SIGN OUT<br/>
                <img id="out" src="{{ URL::asset('assets/img/nav_sprites.png') }}"></a>
        </nav>
    </div>
    @endif
</body>
</html>
