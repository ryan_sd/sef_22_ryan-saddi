<?php

require_once 'Response.php';
require_once 'Validation.php';

/**
* 
*/
class Controller
{
	protected static function checkSet($request, $field)
	{
		if (!array_key_exists($field, $request) || !isset($field)) {
			echo Response::missingParam($field);
			die();
		}
		return [$field => $request[$field]];
	}
}