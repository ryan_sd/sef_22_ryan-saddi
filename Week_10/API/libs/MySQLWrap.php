<?php

require_once '../Config.php';

class MySQLWrap {
	/**
	 * Database Connection Handler
	 *
	 * @var [type]
	 */
	protected $dbCon;
	/**
	 * Alias for mysqli::connect_error
	 *
	 * @var String
	 */
	protected $dbStatus;
	/**
	 * Database we're connected to
	 *
	 * @var [type]
	 */
	protected $dbName;

	/**
	 * [__construct description]
	 */
	public function __construct() {
		// Establish DB connection
		$this->dbCon = new mysqli(DB_HOST, 
		                          DB_USER, 
		                          DB_PASSWORD, 
		                          DB_SCHEMA);
		// Store the schema
		$this->dbName = DB_SCHEMA;
		// Store the connection status in $dbStatus
		$this->dbStatus = $this->dbCon->connect_error;
	}

	/**
	 * [__destruct description]
	 */
	public function __destruct() {
		// Close the open connection to the
		// DB
		$this->dbCon->close();
	}


	/**
	 * [getDBCon description]
	 *
	 * @return [type] [description]
	 */
	public function getDBCon() {
		return $this->dbCon;
	}


	/**
	 * [getDBStatus description]
	 *
	 * @return [type] [description]
	 */
	public function getDBStatus() {
		return $this->dbStatus;
	}

	/**
	 * Get Selected Database Name
	 *
	 * @return [type] [description]
	 */
	public function getDBName() {
		return $this->dbName;
	}

}