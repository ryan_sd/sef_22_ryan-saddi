<?php

/**
* 
*/
class Response
{
	public static function success($toJson)
	{
		http_response_code(200);
		return json_encode($toJson, JSON_PRETTY_PRINT);
	}

	public static function forbidden()
	{
    	http_response_code(401);
		return json_encode([
                        	'code' => 110,
                        	'message' => 'Record was not deleted',
                        	'description' => "Record cannot be deleted",
                    	   ], JSON_PRETTY_PRINT);
	}

	public static function wrongType($field, $pattern)
	{
		http_response_code(400);
		return json_encode([
                        	'code' => 115,
                        	'message' => 'Parameter type mismatch',
                        	'description' => "%field should be $pattern",
                    	   ], JSON_PRETTY_PRINT);
	}

	public static function missingParam($field)
	{
	    http_response_code(400);
		return json_encode([
                        	'code' => 120,
                        	'message' => 'Missing Parameter',
                        	'description' => "the field $field was not sent",
                    	   ], JSON_PRETTY_PRINT);
	}

	public static function notFound($table, $id)
	{
		http_response_code(404);
		return json_encode([
                    		'code' => 125,
                    		'message' => "{$table} not found",
                       		'description' => "{$table} id {$id} does not exist",
                    	   ], JSON_PRETTY_PRINT);
	}
}