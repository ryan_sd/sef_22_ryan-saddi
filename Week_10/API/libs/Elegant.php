<?php

require_once "MySQLWrap.php";

/**
* 
*/
class Elegant
{
	public static function update($table, $idColumn, $id, $data)
	{
		$updates = "";
		foreach ($data as $col => $value) {
			$updates .= "{$col} = '{$value}'";
			$updates .= ", ";
		}
		$updates = trim($updates, ', ');

		$query = "UPDATE $table " .
				 "SET " .
				 "{$updates} " .
				 "WHERE $idColumn = $id";

		$db = new MySQLWrap();
		$dbCon = $db->getDBCon();
		// Execute the query
		$queryResult = $dbCon->query($query);
		if ($queryResult) {
			return $id;
		} else {
			return 0;
		}
	}

	public static function delete($table, $idColumn, $id)
	{
		$query = "DELETE " .
	             "FROM $table " .
	             "WHERE $idColumn = $id";
	    var_dump($query);
		$db = new MySQLWrap();
	    $dbCon = $db->getDBCon();
	    // Execute the query
	    $queryResult = $dbCon->query($query);
		return $dbCon->affected_rows;
	}

	public static function add($table, $json)
	{
		$dataToInsert = array_keys($json);
		$dbDataKeys = implode(', ', $dataToInsert);
		$dbDataVals = implode('\', \'', $json);

		$query = "INSERT INTO $table " . 
				 "({$dbDataKeys}) ".
				 "VALUES " .
				 "('{$dbDataVals}')";

		$db = new MySQLWrap();
	    $dbCon = $db->getDBCon();
	    // Execute the query
	    $queryResult = $dbCon->query($query);
		return $dbCon->insert_id;
	}

	public static function findAll($table)
	{
		$query = "SELECT " .
	             "* " .
	             "FROM $table";
	    return self::executeFind($query);
	}

	public static function find($table, $column, $value)
	{
		$query = "SELECT " .
	             "* " .
	             "FROM $table " .
	             "WHERE $column = $value";

	    $result = self::executeFind($query);
	    if (count($result) > 0) {
	    	return $result;
	    }
	    return null;
	}

	private static function executeFind($query)
	{
		$db = new MySQLWrap();
	    $dbCon = $db->getDBCon();
	    // Execute the query
	    $queryResult = $dbCon->query($query);
	    $stored = array();
	    // Fetch the results
	    if ($queryResult) {
	    	while ($row = $queryResult->fetch_assoc()) {
		        // Create a new Store Object
		        $tmpStore = json_decode(json_encode($row));
		        // StoresArray
		        array_push($stored, $tmpStore);
			}
		    // Close the result query
		    $queryResult->close();
	    }
	    // Return the result
	    return $stored;
	}
}