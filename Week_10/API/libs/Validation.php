<?php

/**
* 
*/
class Validation
{
	private static $patterns = [
	    'numeric'  => '[0-9]+',
	    'word' => '[a-zA-Z]+',
	];

	public static function validate($field, $condition)
	{
		$pattern = self::$patterns[$condition];
		$regex = '#' . $pattern . '#';
		
		if(!preg_match($regex, $field)) {
			Response::wrongType($field, $condition);
		}
	}
}