<?php

/**
* 
*/
class Route
{
	private $routes = [
	    'GET'    => [],
	    'POST'   => [],
	    'PUT'    => [],
	    'PATCH'  => [],
	    'DELETE' => [],
	];

	private $patterns = [
	    ':num'  => '[0-9]+',
	    ':word' => '[a-zA-Z]+',
	];

	public function listRoutes()
	{
		var_dump($this->routes);
	}

	private function routeToRegEx($path)
	{
		// change path to a regular expression
        $regex = '#^' . $path . '$#';
        // replace all symbols to their corresponding patterns
        $regex = str_replace('{:num}' , $this->patterns[':num' ], $regex);
        $regex = str_replace('{:word}', $this->patterns[':word'], $regex);
        return $regex;
	}

	private function findPath($method, $path)
	{
		// if the path has no variables (no patterns)
		if (array_key_exists($path, $this->routes[$method])) {
			return $path;
		} else {
			//otherwise loop on each route
			foreach (array_keys($this->routes[$method]) as $availablePath) {
				//translate the route to a regex
		        $pathReg = $this->routeToRegEx($availablePath);
		        // compare it with path for match
		        if(preg_match($pathReg, $path)) {
		            return $availablePath;
		        }
			}
		}
		// no route matches path
		return "";
	}

	public function executeCallback($method, $path)
	{
		$routeIndex = $this->findPath($method, $path);
		if ($routeIndex != "") {
			// split the path to chunks
			$pathChunked = explode('/', $path);
			$correspondingChunked = explode('/', $routeIndex);
			//find the arguments to pass to the callback function
			$args = array_diff($pathChunked, $correspondingChunked);

			// the corresponding route function
			$handler = $this->routes[$method][$routeIndex];
			// if the function is not anonymous
			if (is_string($handler) && strpos($handler, '@')) {
				$handler = str_replace('@', '::', $handler);
			}
			// if method is a post
			if ($method == 'POST') {
				// add post to arguments
				array_push($args, $_POST);
			// if it's a put or a delete
			} elseif ($method == 'PUT' || $method == 'DELETE' || $method == 'PATCH') {
			    parse_str(file_get_contents("php://input"), $input);
				array_push($args, $input);
			}
			// execute callback function (with its parameters)
			return call_user_func_array($handler, $args);			
		}
  		// return http_response_code(404);
	}

	private function addRoute($method, $path, $callback)
	{
		$this->routes[$method][$path] = $callback;
	}

	public function get($path, $callback)
	{
		$this->addRoute('GET', $path, $callback);
	}

	public function post($path, $callback)
	{
		$this->addRoute('POST', $path, $callback);
	}

	public function put($path, $callback)
	{
		$this->addRoute('PUT', $path, $callback);
	}

	public function patch($path, $callback)
	{
		$this->addRoute('PATCH', $path, $callback);
	}

	public function delete($path, $callback)
	{
		$this->addRoute('DELETE', $path, $callback);
	}
}