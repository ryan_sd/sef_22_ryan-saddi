<?php

require_once 'Elegant.php';

/**
* 
*/
class Model
{

	public static function update($id, $newData)
	{
		$table = self::getTableName();
		$idColumn = self::getIdColumnName();

		$newId = Elegant::update($table, $idColumn, $id, $newData);
		
		if ($newId > 0) {
			return self::find($newId);
		} else {
			return null;
		}
	}

	public static function cascade($col, $val)
	{
		$affectedRows = 0;
		$table = self::getTableName();
		$idColumn = self::getIdColumnName();

		$model = get_called_class();

		if (property_exists($model, 'idColumn') &&
			property_exists($model, 'masterOf')) {
			$using = $model::$masterOf;
			foreach ($using as $dependantModel) {
				$dependantTable = $dependantModel::getTableName();
				$dependantID = $dependantModel::getIdColumnName();
				$dependantResults = Elegant::find($dependantTable, $idColumn, $val);
				if (!is_null($dependantResults)) {
					foreach ($dependantResults as $dependantResult) {
						$deleteID = $dependantResult->{$dependantID};
						$affectedRows += $dependantModel::cascade($col, $deleteID);
					}
				}
				$affectedRows += Elegant::delete($table, $idColumn, $val);
			}
		} else {
			$affectedRows = Elegant::delete($table, $idColumn, $val);
		}
		return $affectedRows;
	}
	
	public static function delete($id)
	{
		$affectedRows = 0;
		$idColumn = self::getIdColumnName();
		$affectedRows += self::cascade($idColumn, $id);

		return $affectedRows;
	}

	public static function singledelete($id)
	{	
		$table = self::getTableName();
		$idColumn = self::getIdColumnName();
		$affectedRows = Elegant::delete($table, $idColumn, $id);
		return $affectedRows;
	}

	public static function add($customerInfo)
	{
		$table = self::getTableName();
		$newId = Elegant::add($table, $customerInfo);
		if ($newId > 0) {
			return self::find($newId);
		} else {
			return null;
		}
	}

	public static function find($id)
	{
		$table = self::getTableName();
		$idColumn = self::getIdColumnName();
		return Elegant::find($table, $idColumn, $id)[0];
	}

	public static function findAll()
	{
		$table = self::getTableName();
		return Elegant::findAll($table);
	}

	private static function getTableName()
	{
		// get model class that called this function
		$model = get_called_class();
		// take the default table name that will be used in the DB
		$table = strtolower($model);
		// if table name is overriden in child class
		if (property_exists($model, 'tableName')) {
			// use it
			$table = $model::$tableName;
		}
		return $table;
	}

	private static function getIdColumnName()
	{
		// get model class that called this function
		$model = get_called_class();
		// take the default column name for the id
		$id = 'id';
		// if col name is overriden in child class
		if (property_exists($model, 'idColumn')) {
			// use it
			$id = $model::$idColumn;
		}
		return $id;
	}
}