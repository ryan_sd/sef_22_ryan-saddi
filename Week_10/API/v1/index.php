<?php
require_once '../libs/Route.php';
require_once 'CustomersController.php';
require_once 'FilmsController.php';
require_once 'AddressesController.php';

$routes = new Route();

//ADDRESSES ROUTE ~~~~

// Retrieve Addresses
$routes->get('/addresses', 'AddressesController@getAddresses');
$routes->get('/addresses/{:num}', 'AddressesController@getAddressFromID');
// Manipulate Addresses
$routes->delete('/addresses/{:num}', 'AddressesController@deleteAddress');
$routes->put('/addresses/{:num}', 'AddressesController@updateAddress');
// Create new address in store
$routes->post('/cities/{:num}/addresses', 'AddressesController@createAddress');

//CUSTOMERS ROUTE ~~~~

// Retrieve Customers
$routes->get('/customers', 'CustomersController@getCustomers');
$routes->get('/customers/{:num}', 'CustomersController@getCustomerFromID');
// Manipulate Customers
$routes->delete('/customers/{:num}', 'CustomersController@deleteCustomer');
$routes->put('/customers/{:num}', 'CustomersController@updateCustomer');
$routes->patch('/customers/{:num}', 'CustomersController@changeActivity');
$routes->patch('/stores/{:num}/customers/{:num}', 'CustomersController@changeStore');
// Create new customer in store
$routes->post('/stores/{:num}/customers', 'CustomersController@createCustomer');

//FILMS ROUTE ~~~~

// Retrieve Films
$routes->get('/films', 'FilmsController@getFilms');
$routes->get('/films/{:num}', 'FilmsController@getFilmFromID');
// Manipulate Films
$routes->delete('/films/{:num}', 'FilmsController@deleteFilm');
$routes->put('/films/{:num}', 'FilmsController@updateFilm');
$routes->patch('/films/{:num}/rating', 'FilmsController@changeRating');
$routes->patch('/films/{:num}/rental', 'FilmsController@changeRentRate');
// Create new film
$routes->post('/films', 'FilmsController@createFilm');


if (isset($_SERVER['REQUEST_METHOD']) && isset($_SERVER['PATH_INFO'])) {
	$method = $_SERVER['REQUEST_METHOD'];
	$request = $_SERVER['PATH_INFO'];
	$result = $routes->executeCallback($method, $request);
}