<?php

require_once 'Customer.php';
require_once 'Store.php';
require_once 'Address.php';
require_once '../libs/Controller.php';

/**
 * 
 */
class CustomersController extends Controller {

    public static function changeStore($storeID, $customerID)
    {
        //check if foreign key exists
        $storeRecord = Store::find($storeID);
        // if not found return error
        if (is_null($storeRecord)) {
            echo Response::notFound('store', $storeID);
        }
        // change store
        self::changeField($customerID, ['store_id' => $storeID]);
    }

    public static function changeActivity($id, $request)
    {
        //check if active field was set
        parent::checkSet($request, 'active');
        // change activity
        self::changeField($id, ['active' => $request['active']]);
    }

    private static function changeField($id, $newData)
    {
        // check if customer exists
        $customerToUpdate = Customer::find($id);

        // if not found return error
        if (is_null($customerToUpdate)) {
            echo Response::notFound('customer', $id);
        // otherwise update
        } else {
            $customer = Customer::update($id, $newData);
            echo Response::success($customer);
        }
    }

    public static function updateCustomer($id, $request)
    {
        $newData = [];
        // check if all the required fields were set
        $newData += parent::checkSet($request, 'store_id');
        $newData += parent::checkSet($request, 'first_name');
        $newData += parent::checkSet($request, 'last_name');
        $newData += parent::checkSet($request, 'email');
        $newData += parent::checkSet($request, 'address_id');
        $newData += parent::checkSet($request, 'active');
        // check if customer exists
        $customerToUpdate = Customer::find($id);
        // check if foreign keys exist
        $addressRecord = Address::find($request['address_id']);
        $storeRecord = Store::find($request['store_id']);

        // if not found return error
        if (is_null($customerToUpdate)) {
            echo Response::notFound('customer', $id);
        } elseif (is_null($addressRecord)) {
            echo Response::notFound('address', $request['address_id']);
        } elseif (is_null($storeRecord)) {
            echo Response::notFound('store', $request['store_id']);
        // otherwise update customer
        } else {
            $customer = Customer::update($id, $newData);
            echo Response::success($customer);
        }
    }

    public static function deleteCustomer($id)
    {
        $affected = Customer::delete($id);
        // if row was deleted
        if ($affected > 0) {
            http_response_code(204);
        // if error occured during deletion
        } elseif ($affected < 0) {
            echo Response::forbidden();
        // if no row was deleted
        } else {
            echo Response::notFound('customer', $id);
        }
    }

    public static function createCustomer($storeID, $request)
    {
        $newData = [];
        // check if all fields were set and add them to newData
        $newData += ['store_id' => $storeID];
        $newData += parent::checkSet($request, 'first_name');
        $newData += parent::checkSet($request, 'last_name');
        $newData += parent::checkSet($request, 'email');
        $newData += parent::checkSet($request, 'address_id');
        $newData += parent::checkSet($request, 'active');

        $addressID = $request['address_id'];

        // check if foreign keys exist
        $addressRecord = Address::find($addressID);
        $storeRecord = Store::find($storeID);

        // if not found return error
        if (is_null($addressRecord)) {
            echo Response::notFound('address', $addressID);
        } else if (is_null($storeRecord)) {
            echo Response::notFound('store', $storeID);
        // otherwise create customer
        } else {
            $customer = Customer::add($newData);
            echo Response::success($customer);
        }
    }

    public static function getCustomers()
    {
        $customers = Customer::findAll();
        echo Response::success($customers);
    }

    public function getCustomerFromID($id)
    {
        $customer = Customer::find($id);
        if (is_null($customer)) {
            echo Response::notFound('customer', $id);
        } else {
            echo Response::success($customer);
        }
    }
}