<?php

require_once '../libs/Model.php';
require_once 'Customer.php';
/**
* 
*/
class Address extends Model
{

	public static $idColumn = 'address_id';

	public static $masterOf = ['Customer'];

}