<?php

require_once 'Film.php';
require_once 'Language.php';
require_once '../libs/Controller.php';

/**
 * 
 */
class FilmsController extends Controller {
    public static function changeRentRate($id, $request)
    {
        //check if rental rate field was set
        parent::checkSet($request, 'rental_rate');
        // change activity
        self::changeField($id, ['rental_rate' => $request['rental_rate']]);
    }

    public static function changeRating($id, $request)
    {
        //check if rating field was set
        parent::checkSet($request, 'rating');
        // change activity
        self::changeField($id, ['rating' => $request['rating']]);
    }

    private static function changeField($id, $newData)
    {
        // check if film exists
        $filmToUpdate = Film::find($id);

        // if not found return error
        if (is_null($filmToUpdate)) {
            echo Response::notFound('film', $id);
        // otherwise update
        } else {
            $film = Film::update($id, $newData);
            echo Response::success($film);
        }
    }

    public static function updateFilm($id, $request)
    {
        $newData = [];

        // check if all the required fields were set
        $newData += parent::checkSet($request, 'title');
        $newData += parent::checkSet($request, 'description');
        $newData += parent::checkSet($request, 'release_year');
        $newData += parent::checkSet($request, 'language_id');
        $newData += parent::checkSet($request, 'rental_duration');
        $newData += parent::checkSet($request, 'rental_rate');
        $newData += parent::checkSet($request, 'length');
        $newData += parent::checkSet($request, 'replacement_cost');
        $newData += parent::checkSet($request, 'rating');
        $newData += parent::checkSet($request, 'special_features');

        // check if film exists
        $filmToUpdate = Film::find($id);
        // check if foreign keys exist
        $languageRecord = Language::find($request['language_id']);

        // if not found return error
        if (is_null($languageRecord)) {
            echo Response::notFound('language', $request['language_id']);
        } elseif (is_null($filmToUpdate)) {
            echo Response::notFound('film', $id);
        // otherwise update film
        } else {
            $film = Film::update($id, $newData);
            echo Response::success($film);
        }
    }

    public static function deleteFilm($id)
    {
        $affected = Film::delete($id);
        // if row was deleted
        if ($affected > 0) {
            http_response_code(204);
        // if error occured during deletion
        } elseif ($affected < 0) {
            echo Response::forbidden();
        // if no row was deleted
        } else {
            echo Response::notFound('film', $id);
        }
    }

    public static function createFilm($request)
    {
        $newData = [];
        // check if all fields were set and add them to newData
        $newData += parent::checkSet($request, 'title');
        $newData += parent::checkSet($request, 'description');
        $newData += parent::checkSet($request, 'release_year');
        $newData += parent::checkSet($request, 'language_id');
        $newData += parent::checkSet($request, 'rental_duration');
        $newData += parent::checkSet($request, 'rental_rate');
        $newData += parent::checkSet($request, 'length');
        $newData += parent::checkSet($request, 'replacement_cost');
        $newData += parent::checkSet($request, 'rating');
        $newData += parent::checkSet($request, 'special_features');

        // check if foreign keys exist
        $languageRecord = Language::find($request['language_id']);

        // if not found return error
        if (is_null($languageRecord)) {
            echo Response::notFound('language', $request['language_id']);
        // otherwise create film
        } else {
            $film = Film::add($newData);
            echo Response::success($film);
        }
    }

    public static function getFilms()
    {
        $films = Film::findAll();
        echo Response::success($films);
    }

    public function getFilmFromID($id)
    {
        $film = Film::find($id);
        if (is_null($film)) {
            echo Response::notFound('film', $id);
        } else {
            echo Response::success($film);
        }
    }
}