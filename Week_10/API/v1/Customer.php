<?php

require_once '../libs/Model.php';
require_once 'Payment.php';
require_once 'Rental.php';

class Customer extends Model {

	public static $idColumn = 'customer_id';
	
	public static $masterOf = ['Payment', 'Rental'];
}