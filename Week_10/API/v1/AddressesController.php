<?php

require_once 'Address.php';
require_once 'City.php';
require_once '../libs/Controller.php';

/**
 * 
 */
class AddressesController extends Controller {

    public static function updateAddress($id, $request)
    {
        $newData = [];
        // check if all the required fields were set
        $newData += parent::checkSet($request, 'address');
        $newData += parent::checkSet($request, 'address2');
        $newData += parent::checkSet($request, 'district');
        $newData += parent::checkSet($request, 'city_id');
        $newData += parent::checkSet($request, 'postal_code');
        $newData += parent::checkSet($request, 'phone');

        // check if address exists
        $addressToUpdate = Address::find($id);

        // check if foreign key exist
        $cityRecord = City::find($request['city_id']);

        // if not found return error
        if (is_null($addressToUpdate)) {
            echo Response::notFound('address', $id);
        } elseif (is_null($cityRecord)) {
            echo Response::notFound('city', $request['city_id']);
        // otherwise update address
        } else {
            $address = Address::update($id, $newData);
            echo Response::success($address);
        }
    }

    public static function deleteAddress($id)
    {
        $affected = Address::delete($id);
        // if row was deleted
        if ($affected > 0) {
            http_response_code(204);
        // if error occured during deletion
        } elseif ($affected < 0) {
            echo Response::forbidden();
        // if no row was deleted
        } else {
            echo Response::notFound('address', $id);
        }
    }

    public static function createAddress($cityID, $request)
    {
        $newData = [];
        // check if all fields were set and add them to newData
        $newData += parent::checkSet($request, 'address');
        $newData += parent::checkSet($request, 'address2');
        $newData += parent::checkSet($request, 'district');
        $newData += parent::checkSet($request, 'postal_code');
        $newData += parent::checkSet($request, 'phone');
        $newData += ['city_id' => $cityID];

        // check if foreign keys exist
        $cityRecord = City::find($cityID);

        // if not found return error
        if (is_null($cityRecord)) {
            echo Response::notFound('city', $cityID);
        // otherwise create address
        } else {
            $address = Address::add($newData);
            echo Response::success($address);
        }
    }

    public static function getAddresses()
    {
        $addresses = Address::findAll();
        echo Response::success($addresses);
    }

    public function getAddressFromID($id)
    {
        $address = Address::find($id);
        if (is_null($address)) {
            echo Response::notFound('address', $id);
        } else {
            echo Response::success($address);
        }
    }
}