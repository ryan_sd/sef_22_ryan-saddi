<?php

require_once '../libs/Model.php';

class Store extends Model {
	public static $tableName = 'store';
	public static $idColumn = 'store_id';
}